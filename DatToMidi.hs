{-# LANGUAGE Trustworthy #-}
{-
  Copyright 2010–2015 Chiraag M. Nataraj
  This file is part of Musik.
  
  Musik is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

-- |DatToMidi converts 'Dat' to a representation of MIDI.

module DatToMidi
    (
     -- *Datatypes
     MidiWrapper(..)
    ) where
    
    import Common
    import Constants
    import Datatypes

    import Codec.Midi
    import qualified Data.Foldable as F
    import Data.Maybe
    import Data.Ord
    import Data.List
    import qualified Data.DList as D
    import qualified Data.Sequence as S

    dto :: Int
    dto = 0
        
    restPitch :: Int
    restPitch = 0

    allChannels :: [Int]
    allChannels = [0..15]

    channelSplit :: Int
    channelSplit = 8

    chordChannel :: Int
    chordChannel = 15

    initChannel :: Int
    initChannel = 0

    controllerOn :: Int
    controllerOn = 127

    controllerOff :: Int
    controllerOff = 0

    beatChannel :: Int
    beatChannel = 9

    legatoController :: Int
    legatoController = 64

    staccatoController :: Int
    staccatoController = 75

    initTempo :: Int
    initTempo = 120
                         
    tempoConst :: Int
    tempoConst = 240000000

    velocityOn :: Int
    velocityOn = 64

    velocityOff :: Int
    velocityOff = 0

    -- |'MidiWrapper' is a newtype wrapper around Midi.
        
    newtype MidiWrapper = MidiWrapper { extractMidi :: Midi }

    -- |Implements the 'Midi' instance of 'Musik' using
    -- 'processToMidi' and 'exportFile'.
                     
    instance Musik MidiWrapper where
            process = MidiWrapper . processToMIDI
            export = flip (.) extractMidi . exportFile

    surroundWithController :: Channel -> Dat -> Int -> D.DList (Int, Message)
    surroundWithController chan stuff cont =
        let conton = (dto, ControlChange chan cont controllerOn)
            dat = convertDatToMIDI chan stuff
            contoff = (dto, ControlChange chan cont controllerOff)
        in
          D.snoc (D.cons conton dat) contoff

    -- 'convertDatToMIDI' converts a 'Int' and 'Dat' into a MIDI message.
                       
    convertDatToMIDI :: Int -> Dat -> D.DList (Int, Message)
    convertDatToMIDI chan (Note pitch dur) =
        let noteon = (dto, NoteOn chan pitch (ifthenelse velocityOn velocityOff (pitch /= restPitch)))
            noteoff = (round (fromIntegral tpb * dur), NoteOff chan pitch velocityOff)
            otherChannels = filter (/= chan) allChannels
            copyToOtherChannels x = map (\i -> (dto, x i restPitch velocityOff)) otherChannels
        in
          D.append
           (D.fromList (noteon : copyToOtherChannels NoteOn))
           (D.fromList (noteoff : copyToOtherChannels NoteOff))
    convertDatToMIDI chan (ListDat l) = foldr (D.append . convertDatToMIDI chan) D.empty l
    convertDatToMIDI _ (Tempo tmp) = D.singleton (dto, TempoChange (div tempoConst tmp))
    convertDatToMIDI _ (Simul stuff) =
        let chanList = ifthenelse
                       [0..length stuff - 1]
                       ([0..8] ++ [10..length stuff - 1])
                       (length stuff <= channelSplit)
            x =
                zipWith
                (\c -> filterChannel c True . convertDatToMIDI c)
                chanList
                stuff
        in
          (D.fromList . fromAbsTime . sortBy (comparing fst) .
            concatMap toAbsTime . fmap F.toList) x
    convertDatToMIDI _ (Instrument chan inst) =
        D.singleton (dto, ProgramChange chan inst)
    convertDatToMIDI chan (Chord (ListDat (first:chord)) stuff) =
        let
            x = ((.) (filterChannel chordChannel False) . convertDatToMIDI)
                chan stuff
            (Note firstp firstd) = first
            others n v = fmap
                          (\(Note p _) -> (dto, n chordChannel p v))
                          (D.fromList chord)
            cOn = (dto, NoteOn chordChannel firstp velocityOn)
            othersOn = others NoteOn velocityOn
            cOff = (round (fromIntegral tpb * firstd),
                    NoteOff chordChannel firstp velocityOff)
            othersOff = others NoteOff velocityOff
            c = D.append
                 (D.cons cOn othersOn)
                 (D.cons cOff othersOff)
        in
          D.fromList
           (fromAbsTime
            (sortBy (comparing fst)
             ((toAbsTime . F.toList) x ++ (toAbsTime . F.toList) c)))
    convertDatToMIDI chan (Tuplet stuff) = convertDatToMIDI chan stuff
    convertDatToMIDI chan (Repeat t stuff) =
        (F.asum . replicate t . convertDatToMIDI chan) stuff
    convertDatToMIDI chan (Legato stuff) =
        surroundWithController
        chan
        stuff
        legatoController
    convertDatToMIDI chan (Staccato stuff) =
        surroundWithController
        chan
        stuff
        staccatoController
    convertDatToMIDI track (Beat b n) =
        let d = calcDuration n
            times = round (d / calcDuration b) :: Int
            x = ((.) (filterChannel beatChannel False) .
                 convertDatToMIDI) track n
            tmp = ((.) (filterChannel beatChannel True) .
                   convertDatToMIDI) beatChannel b
            y = (F.asum . replicate times) tmp
        in
          D.fromList
           (fromAbsTime
            (sortBy (comparing fst)
             ((toAbsTime . F.toList) x ++ (toAbsTime . F.toList) y)))
    convertDatToMIDI _ _ = D.empty

    -- 'filterChannel' takes a list of MIDI Messages and filters the
    -- ones that either are or are not in a particular channel.
                               
    filterChannel :: Channel -> Bool -> D.DList (Int, Message) -> D.DList (Int, Message)
    filterChannel c b = D.fromList . filter ((==) b . flip compareChannel c . snd) . F.toList

    -- 'compareChannel' takes a MIDI Message, extracts its channel,
    -- and compares it with the given channel.
                    
    compareChannel :: Message -> Channel -> Bool
    compareChannel (NoteOn c _ _) = (c ==)
    compareChannel (NoteOff c _ _) = (c ==)
    compareChannel (ProgramChange c _) = (c ==)
    compareChannel (ControlChange c _ _) = (c ==)
    compareChannel _ = const True

    -- 'processToMIDI' takes a list of 'FunctionReturn'
    -- and processes it into Midi.
        
    processToMIDI :: S.Seq FunctionReturn -> Midi
    processToMIDI ret =
        let
            trackStart = (dto, TempoChange (div tempoConst initTempo))
            trackEnd = (dto, TrackEnd)
            melody = (trackStart `D.cons`
                      convertDatToMIDI
                      initChannel
                      (ListDat ((catMaybes . F.toList) ret))) `D.snoc`
                     trackEnd
        in Midi {fileType = SingleTrack
                ,timeDiv = TicksPerBeat tpb
                ,tracks = [F.toList melody]}
