\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}Installing Musik}{2}{section.2}
\contentsline {section}{\numberline {3}Using Musik}{2}{section.3}
\contentsline {subsection}{\numberline {3.1}Basic notes and durations}{2}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Commands}{2}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}addBeatMap}{3}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}addInstrument}{3}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}beat}{3}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}chord}{3}{subsubsection.3.2.4}
\contentsline {subsubsection}{\numberline {3.2.5}file}{3}{subsubsection.3.2.5}
\contentsline {subsubsection}{\numberline {3.2.6}import}{3}{subsubsection.3.2.6}
\contentsline {subsubsection}{\numberline {3.2.7}instrument}{4}{subsubsection.3.2.7}
\contentsline {subsubsection}{\numberline {3.2.8}legato}{4}{subsubsection.3.2.8}
\contentsline {subsubsection}{\numberline {3.2.9}mode}{4}{subsubsection.3.2.9}
\contentsline {subsubsection}{\numberline {3.2.10}newSection}{4}{subsubsection.3.2.10}
\contentsline {subsubsection}{\numberline {3.2.11}noPromptFile}{4}{subsubsection.3.2.11}
\contentsline {subsubsection}{\numberline {3.2.12}repeat}{4}{subsubsection.3.2.12}
\contentsline {subsubsection}{\numberline {3.2.13}saveOnly}{5}{subsubsection.3.2.13}
\contentsline {subsubsection}{\numberline {3.2.14}scale}{5}{subsubsection.3.2.14}
\contentsline {subsubsection}{\numberline {3.2.15}section}{5}{subsubsection.3.2.15}
\contentsline {subsubsection}{\numberline {3.2.16}simul}{5}{subsubsection.3.2.16}
\contentsline {subsubsection}{\numberline {3.2.17}slur}{5}{subsubsection.3.2.17}
\contentsline {subsubsection}{\numberline {3.2.18}soundbank}{5}{subsubsection.3.2.18}
\contentsline {subsubsection}{\numberline {3.2.19}staccato}{5}{subsubsection.3.2.19}
\contentsline {subsubsection}{\numberline {3.2.20}tempo}{6}{subsubsection.3.2.20}
\contentsline {subsubsection}{\numberline {3.2.21}transpose}{6}{subsubsection.3.2.21}
\contentsline {subsubsection}{\numberline {3.2.22}tuplet}{6}{subsubsection.3.2.22}
\contentsline {section}{\numberline {4}Running Musik}{6}{section.4}
\contentsline {section}{\numberline {A}Tables}{6}{Appendix.1.A}
