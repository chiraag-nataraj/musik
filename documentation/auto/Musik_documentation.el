(TeX-add-style-hook
 "Musik_documentation"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("cleveref" "capitalize" "noabbrev") ("geometry" "top=1in" "bottom=1in" "left=1in" "right=1in")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "hyperref"
    "booktabs"
    "longtable"
    "parskip"
    "appendix"
    "cleveref"
    "geometry")
   (LaTeX-add-labels
    "sec:run"
    "tab:durations"
    "tab:beats"
    "tab:chords"
    "tab:modes"
    "tab:insts")))

