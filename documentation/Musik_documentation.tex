\documentclass{article}
\usepackage{hyperref}
\usepackage{booktabs}
\usepackage{longtable}
\usepackage{parskip}
\usepackage{appendix}
% \usepackage{inconsolata}
\usepackage[capitalize,noabbrev]{cleveref}
\usepackage[top=1in,bottom=1in,left=1in,right=1in]{geometry}

\hypersetup{hidelinks}
\begin{document}
\title{Musik: the Open Source Text-to-Music converter}
\author{Chiraag Nataraj}
\maketitle
\tableofcontents
\pagebreak
\section{Introduction}
Thank you for choosing to use Musik, a text-to-music converter.  Musik uses HCodecs (\url{https://hackage.haskell.org/package/HCodecs}) and Haskell to accomplish its task.  Read ahead to find out how to install and use Musik.
\section{Installing Musik}
To install Musik, simply download the appropriate binary and put it somewhere convenient. \texttt{i386} designates a 32-bit binary and \texttt{x86\_64} designates a 64-bit binary.

\section{Using Musik}
Musik has been designed with musicians in mind, with simple syntax.  To start out, open up a new text file in your favorite text editor (Notepad, Notepad++, Text Editor, Gedit, Kedit, etc.).  Musik files are denoted by the extension musik.  However, just like files created by \LaTeX\,and Lilypond, by which this project was inspired, they are just text files.  There are two basic types of text recognizable to Musik:
\begin{enumerate}
\item Notes - these are just plain notes with no special syntax.
\item Commands - these are begun with a \verb|\| and tell Musik that the following text should mean something special to it.
\end{enumerate}
\subsection{Basic notes and durations}
To enter notes, simply enter the note name followed by the duration. Valid durations are listed in \cref{tab:durations}. A rest is signified by the letter ``R''.  For example, this sequence will play the C major scale with a quarter note for every beat:
\begin{verbatim}
C4 D4 E4 F4 G4 A4 B4 C^4
\end{verbatim}
However, this sequence will play the same scale with alternating quarter and eighth notes:
\begin{verbatim}
C4 D8 E4 F8 G4 A8 B4 C^8
\end{verbatim}
To tie notes together, all that is needed is a \verb|-| between each duration.  For example, to hold a middle C note for a whole note and then a quarter note, all you need to do is type the following:

\begin{verbatim}
C1-4
\end{verbatim}
and Musik does the rest!

$\#$ will raise the note a half-step (\verb|C| $\rightarrow$ \verb|C|$\sharp$).  b will lower the note a half-step (\verb|B| $\rightarrow$ \verb|B|$\flat$). \verb{^{ will raise the note an octave, and $\_$ will lower it an octave.

\emph{Note:} All notes \emph{must} be capital letters (e.g. \verb|C| rather than \verb|c|).

\subsection{Commands}

All available commands are documented here in full in alphabetical order.

\subsubsection{addBeatMap}

Syntax:

\begin{verbatim}
\addBeatMap{symbol}{note}
\end{verbatim}

Creates a mapping from \verb|symbol| to \verb|note| in the beat map. \verb|symbol| here can be any sequence of letters. \verb|note| is the note that should correspond to that symbol. See \verb|beat| for more information and the default beat map.

\emph{Note:} While \verb|note| should have a duration, the duration does not matter and will be discarded.

\subsubsection{addInstrument}

Syntax:

\begin{verbatim}
\addInstrument{name}{number}
\end{verbatim}

Creates a mapping from \verb|name| to \verb|number| in the instrument map. This is highly soundbank dependent, so if you create a custom mapping, make sure that patch number exists in your soundbank! Otherwise you will not hear anything. See \verb|instrument| for the default instrument map.

\subsubsection{beat}

Syntax:

\begin{verbatim}
\beat{beat sequence}{stuff}
\end{verbatim}

The beat sequence consists of certain beat \verb|symbol|s as given in \cref{tab:beats} (along with any custom mappings defined with \verb|\addBeatMap|) along with their durations.

\subsubsection{chord}

Syntax:

\begin{verbatim}
\chord{chord_notes}{stuff}
\end{verbatim}

\verb|chord_notes| can consist of a note and a predefined chord as given in \cref{tab:chords} or can be manually defined by separating notes with \verb|+|.

\subsubsection{file}

Syntax:

\begin{verbatim}
\file{file to save to}
\end{verbatim}

This command is fairly self-explanatory --- it sets the output file name. One thing to keep in mind is that the extension is automatically appended (``.mid'').

\subsubsection{import}

Syntax:

\begin{verbatim}
\import{file to import}
\end{verbatim}

This command is also fairly self-explanatory --- it imports the referenced Musik file. Paths are relative if a full path is not specified.

\subsubsection{instrument}

Syntax:

\begin{verbatim}
\instrument[channel]{instrument}
\end{verbatim}

This command sets the instrument for the specified channel. Channels are numbered from 0 -- 15. Note that chords are played on channel 15. See \cref{tab:insts} for the instruments defined by default.

\subsubsection{legato}

Syntax:

\begin{verbatim}
\legato{stuff}
\end{verbatim}

This command simulates holding the sustain pedal.

\subsubsection{mode}

Syntax:

\begin{verbatim}
\mode{mode}
\end{verbatim}

This command automatically sets sharps and flats according to \verb|mode|. The modes supported are listed in \cref{tab:modes}.

\subsubsection{newSection}

Syntax:

\begin{verbatim}
\newSection[name]{stuff}
\end{verbatim}

\verb|name| is the name of the section (the label you will use to refer to it). \verb|stuff| is the content of the section.

\subsubsection{noPromptFile}

Syntax:

\begin{verbatim}
\noPromptFile{filename}
\end{verbatim}

This is an alias to \verb|\file|.

\subsubsection{repeat}

Syntax:

\begin{verbatim}
\repeat[times]{stuff}
\end{verbatim}

This command is fairly self-explanatory --- it repeats \verb|stuff| \verb|times| times.

\subsubsection{saveOnly}

Syntax:

\begin{verbatim}
\saveOnly
\end{verbatim}

This command used to exist, but has been disabled for now. It may be re-enabled in a future release and is hence documented here. Using it will result in a warning and the command will be ignored.

\subsubsection{scale}

Syntax:

\begin{verbatim}
\scale{scale}
\end{verbatim}

This command is an alias for \verb|\mode|.

\subsubsection{section}

Syntax:

\begin{verbatim}
\section{name}
\end{verbatim}

This command will recall the section with the name \verb|name| that was created with \verb|\newSection|.

\subsubsection{simul}

Syntax:

\begin{verbatim}
\simul{stuff1}[{stuff2}{stuff3}...]
\end{verbatim}

This command allows playing up to 14 different things simultaneously. Note that since this command uses different channels and two channels are reserved (channel 9 for beats and channel 15 for chords), the number of arguments is limited to 14.

\subsubsection{slur}

\begin{verbatim}
\slur{stuff}
\end{verbatim}

This is an alias to \verb|\legato|.

\subsubsection{soundbank}

Syntax:

\begin{verbatim}
\soundbank{path to soundbank}
\end{verbatim}

This command used to exist, but has been disabled for now. It may be re-enabled in a future release and is hence documented here. Using it will result in a warning and the command will be ignored.

\subsubsection{staccato}

Syntax:

\begin{verbatim}
\staccato{stuff}
\end{verbatim}

This command renders \verb|stuff| in a staccato manner.

\subsubsection{tempo}

Syntax:

\begin{verbatim}
\tempo{temp}
\end{verbatim}

This command sets the tempo to \verb|temp| bpm.

\subsubsection{transpose}

Syntax:

\begin{verbatim}
\transpose{steps}
\end{verbatim}

This command transposes the keys \verb|steps| steps. If \verb|steps| is positive, keys are shifted to the left. If \verb|steps| is negative, keys are shifted to the right.

\subsubsection{tuplet}

Syntax:

\begin{verbatim}
\tuplet{stuff}
\end{verbatim}

This command implements the construct known as ``tuplets'' in Western music.

% \subsection{Tuplets}

% To play a tuplet, simply use the following syntax:
% \begin{verbatim}
% \tuplet{notes to play}
% \end{verbatim}

% Yes, it's \MakeUppercase{that} simple!  For example, to play a triplet of quarter notes, I would type the following:

% \begin{verbatim}
% \tuplet{C4 D4 E4}
% \end{verbatim}

% \subsection{Chords}

% For chords, use the following syntax:
% \begin{verbatim}
% \chord[chord_name]{notes to play while chord is playing}
% \end{verbatim}
% Different \verb{chord_name{ values:
% \newline
% \begin{center}
%   \begin{tabular}{l l}
%     \toprule
%     Value & Meaning \\
%     \midrule
%     M & major \\
%     m & minor \\
%     M7 & major $7^{th}$ \\
%     m7 & minor $7^{th}$ \\
%     dim & diminished\\
%     sus & sustained\\
%     aug & augmented\\
%     aug7 & augmented $7^{th}$ \\
%     add9 & add9 chord \\
%     \bottomrule
%   \end{tabular}
% \end{center}
% For example, to play the C major chord while playing the C Major scale, you would use the following sequence:
% \begin{verbatim}
% \chord[CM]{C4 D4 E4 F4 G4 A4 B4 C^4}
% \end{verbatim}
% \verb{^{ and $\_$ work the same way for chords as they do for individual notes.  For example, to modify the above sequence to play a C major chord in the fourth octave (one below middle C) would be:
% \begin{verbatim}
% \chord[C_M]{C4 D4 E4 F4 G4 A4 B4 C^4}
% \end{verbatim}
% You can also construct your own chords by replacing \verb|chord_name| with the notes you want played separated by a ``+'' sign.  For example, to play C, E, and G (C major) during the scale above, you would put:
% \begin{verbatim}
% \chord[C+E+G]{C4 D4 E4 F4 G4 A4 B4 C^4}
% \end{verbatim}

% \subsection{Instruments}

% To change the ``voice'', or instrument, simply put \verb|\instrument{instrument_name}| or \newline
% \verb|\instrument[channel]{instrument_name}|, replacing \verb|instrument_name| with the name of the desired instrument.  Chords are played on channel 15, so to change the chord instrument, one could do \newline \verb|\instrument[15]{instrument_name}|. The list of instruments supported by default is listed in \cref{tab:insts}.

% \subsection{Sections}

% You can also use sections to simplify typing sections.  Simply use the \verb|\newSection| command like so:
% \begin{verbatim}
% \newSection[name_of_new_section]{notes_to_play_during_recall_of_section}
% \end{verbatim}
% This can be extremely useful if one wants to repeat a certain pattern many times (such as a refrain).  One only needs to code it once and then recall the section with the \verb|\section| command like so:
% \begin{verbatim}
% \section{name_of_section_to_recall}
% \end{verbatim}

% \subsection{Scale changes}

% A really awesome feature of Musik is scale changes.  Don't know the sharps and flats of that one really nice scale?  No worries!  Just use
% \begin{verbatim}
% \scale{scale_name}
% \end{verbatim}
% and Musik will do the rest!  Type the notes like the piece is in C major (a.k.a don't use any sharp or flat signs) and Musik will automagically put the sharps and flats there for you!

% \subsection{Setting the tempo}

% Another feature in this version of Musik is setting the tempo.  Simply type in:
% \begin{verbatim}
% \tempo{tempo}
% \end{verbatim}
% to set the tempo.  % Please note that you \emph{must} specify the tempo in the master file.  \emph{Do not try to set the tempo in the ``slave'' files - you will just become frustrated that some of the other commands (like the instrument command, for example) stop working}.

% \subsection{Slurring}

% Yet another feature of Musik is being able to use the sustain pedal.  All you have to do is:
% \begin{verbatim}
% \slur{notes to slur}
% \end{verbatim}
% and Musik will play those notes as if the sustain pedal is held down. A synonym for this command is \verb|\legato{notes to slur}|.

% \subsection{Comments}

% If you want to comment a line out so that Musik doesn't parse it, simply place a \verb|#| at the beginning of the line.

% \subsection{Useful commands for saving files}

% % Musik has the ability to prevent Musik from actually playing the file and just asking you to save it.  In order to do this, you simply insert the command \verb|\saveOnly| somewhere in your Musik file.  Upon running Musik, you will simply be asked where to save the wav file.

% % To take this further, you can even specify where to store the wav file.  Simply use the command 

% % \verb|\noPromptFile{path_to_wav_file}|.  For example, to store the wav file to 

% % \verb|C:\\Documents and Settings\user\Desktop| as test.wav, I would type the following somewhere in my Musik file:

% % \begin{verbatim}
% % \noFilePrompt{C:\\Documents and Settings\user\Desktop\test.wav}
% % \end{verbatim}
% % With this option, Musik will silently create the wav file.  This is great for creating many Musik files one after another.

% If one does not specify a file with the \verb|\noPromptFile{file}| or \verb|\file{file}| command, the default output file is \verb|out.mid|. Relative paths are supported. Note, however, that the paths are relative to the working directory from which the program is called, \emph{not} with respect to the directory in which the file is stored.

% % \subsection{Soundbanks}

% % Musik now supports the option to import a soundbank \emph{for that specific file}.  To do that, simply add the following in your Musik file:

% % \begin{verbatim}
% % \soundbank{FULL/PATH/TO/SOUNDBANK/FILE}
% % \end{verbatim}

% % Please note that, as of now, Java \emph{does not} like relative paths, so you must specify the \emph{\sc full path} to the soundbank file.  Note that this also applies to the \verb|\noPromptFile| command above.

% \subsection{Split up your music logically}

% I have also introduced the option to split up your music into multiple files.  To do this, write each part in a different Musik file and simply add this line to one ``master'' Musik file

% \begin{verbatim}
% \import{FULL/PATH/TO/MUSIK/FILE}
% \end{verbatim}

% % Please note that when a file is imported like this, the \verb|\saveOnly|, \verb|\noPromptFile|, and \verb|\soundbank| commands do not work.  You \emph{must} specify these in the \emph{master} file.

% % Just as a note, Musik now also writes a log for each file or master file that it parses (the log for each of the ``slave'' files is included in the one written for the master file).

% \subsection{Repeat with ease}

% Musik now supports a repeat command.  It allows one to repeat a set part of music \(n\) times.  The syntax is as follows:

% \begin{verbatim}
% \repeat[n]{music}
% \end{verbatim}

% For example,

% \begin{verbatim}
% \repeat[2]{C4 D4 E4}
% \end{verbatim}

% will play the quarter notes \verb|C|, \verb|D|, and \verb|E| in succession twice.

% % \subsection{``Live mode''}

% % Musik now supports a ``live mode'' where the user can experiment with different chord combinations, scale changes, etc.  If you (the user) start up Musik in the ``normal'' way (see Section \ref{sec:run}), you will get a prompt asking if you would like to go into live mode.  Simply click ``Yes'' and off you go!  The only thing to keep in mind is that Musik does not store music from previous lines.

\section{Running Musik}\label{sec:run}

Just as a demo, copy this sequence into a text file \textbf{on the Desktop} and save it as test.musik.
\begin{verbatim}
\chord[CM]{C4 D8 E4 F8 G4 A8 B4 C^8}
\end{verbatim}

Now open up either the Terminal (Linux and OSX) or the Command-Line (Windows) and type in the following:

\begin{verbatim}
/path/to/musik ~/Desktop/test.musik (Linux/OSX)
\path\to\musik %HOMEPATH%\Desktop\test.musik (Windows)
\end{verbatim}

For example, if \texttt{musik} is saved in your Downloads folder, the command would be
\begin{verbatim}
~/Downloads/musik ~/Desktop/test.musik (Linux/OSX)
%HOMEPATH%\Downloads\musik %HOMEPATH%\Desktop\test.musik (Windows)
\end{verbatim}

The output will be saved to \verb|out.mid|.

\begin{appendices}
  \section{Tables}
  \begin{longtable}[c]{l l}
    \toprule
    Duration & Significance \\
    \midrule
    1 & whole note \\
    2 & half note \\
    4 & quarter note \\
    8 & eighth note \\
    16 & sixteenth note \\
    32 & thirty-second note \\
    64 & sixty-fourth note \\
    128 & one-twenty-eighth note \\
    \bottomrule
    \\
    \caption{List of valid durations}
    \label{tab:durations}
  \end{longtable}
  \begin{longtable}[c]{l l l}
    \toprule
    Instrument & Symbol & ``Note'' \\
    \midrule
    \endhead
    \bottomrule
    \endfoot
    \bottomrule
    \\
    \caption{Default beat map}
    \label{tab:beats}
    \endlastfoot
    Acoustic Bass Drum & abd & 35 \\
    Bass Drum & bd & 36 \\
    Side Stick & ss & 37 \\
    Acoustic Snare & as & 38 \\
    Hand Clap & hc & 39 \\
    Electric Snare & es & 40 \\
    Low Floor Tom & lft & 41 \\
    Closed Hi-Hat & chh & 42 \\
    High Floor Tom & hft & 43 \\
    Pedal Hi-Hat & phh & 44 \\
    Low Tom & lt & 45 \\
    Open Hi-Hat & ohh & 46 \\
    Low-Mid Tom & lmt & 47 \\
    High-Mid Tom & hmt & 48 \\
    Crash Cymbal 1 & cco & 49 \\
    High Tom & ht & 50 \\
    Ride Cymbal 1 & rco & 51 \\
    Chinese Cymbal & chc & 52 \\
    Ride Bell & rb & 53 \\
    Tambourine & t & 54 \\
    Splash Cymbal & sc & 55 \\
    Cowbell & c & 56 \\
    Crash Cymbal 2 & cct & 57 \\
    Vibraslap & v & 58 \\
    Ride Cymbal 2 & rct & 59 \\
    High Bongo & hb & 60 \\
    Low Bongo & lb & 61 \\
    Mute High Conga & mhc & 62 \\
    Open High Conga & ohc & 63 \\
    Low Conga & lc & 64 \\
    High Timbale & hti & 65 \\
    Low Timbale & lti & 66 \\
    High Agogo & ha & 67 \\
    Low Agogo & la & 68 \\
    Cabasa & ca & 69 \\
    Maracas & m & 70 \\
    Short Whistle & sw & 71 \\
    Long Whistle & lw & 72 \\
    Short Guiro & sg & 73 \\
    Long Guiro & lg & 74 \\
    Claves & cl & 75 \\
    High Wood Block & hwb & 76 \\
    Low Wood Block & lwb & 77 \\
    Mute Cuica & mc & 78 \\
    Open Cuica & oc & 79 \\
    Mute Triangle & mt & 80 \\
    Open Triangle & ot & 81
  \end{longtable}

  \begin{longtable}[c]{l l}
    \toprule
    Value & Meaning \\
    \midrule
    M & major \\
    m & minor \\
    M7 & major $7^{th}$ \\
    m7 & minor $7^{th}$ \\
    dim & diminished\\
    sus & sustained\\
    aug & augmented\\
    aug7 & augmented $7^{th}$ \\
    add9 & add9 chord \\
    \bottomrule
    \\
    \caption{Predefined chords}
    \label{tab:chords}
  \end{longtable}

  \begin{longtable}[c]{l l}
    \toprule
    Symbol & Mode \\
    \midrule
    \endhead
    \bottomrule
    \endfoot
    \bottomrule
    \\
    \caption{Supported modes}
    \label{tab:modes}
    \endlastfoot
    M & Major \\
    m & Minor \\
    Ae & Æolian \\
    Lo & Locrian \\
    Io & Ionian \\
    Do & Dorian \\
    Ph & Phrygian \\
    Ly & Lydian \\
    Mi & Mixolydian
  \end{longtable}

  \begin{longtable}[c]{l l}
    \toprule
    Instrument & Number \\
    \midrule
    \endhead
    \bottomrule
    \endfoot
    \bottomrule
    \\
    \caption{List of default instruments}
    \label{tab:insts}
    \endlastfoot
    PIANO & 0 \\
    ACOUSTIC\_GRAND & 0 \\
    BRIGHT\_ACOUSTIC & 1 \\
    ELECTRIC\_GRAND & 2 \\
    HONKEY\_TONK & 3 \\
    ELECTRIC\_PIANO & 4 \\
    ELECTRIC\_PIANO\_1 & 4 \\
    ELECTRIC\_PIANO\_2 & 5 \\
    HARPISCHORD & 6 \\
    CLAVINET & 7 \\
    CELESTA & 8 \\
    GLOCKENSPIEL & 9 \\
    MUSIC\_BOX & 10 \\
    VIBRAPHONE & 11 \\
    MARIMBA & 12 \\
    XYLOPHONE & 13 \\
    TUBULAR\_BELLS & 14 \\
    DULCIMER & 15 \\
    DRAWBAR\_ORGAN & 16 \\
    PERCUSSIVE\_ORGAN & 17 \\
    ROCK\_ORGAN & 18 \\
    CHURCH\_ORGAN & 19 \\
    REED\_ORGAN & 20 \\
    ACCORDIAN & 21 \\
    HARMONICA & 22 \\
    TANGO\_ACCORDIAN & 23 \\
    GUITAR & 24 \\
    NYLON\_STRING\_GUITAR & 24 \\
    STEEL\_STRING\_GUITAR & 25 \\
    ELECTRIC\_JAZZ\_GUITAR & 26 \\
    ELECTRIC\_CLEAN\_GUITAR & 27 \\
    ELECTRIC\_MUTED\_GUITAR & 28 \\
    OVERDRIVEN\_GUITAR & 29 \\
    DISTORTION\_GUITAR & 30 \\
    GUITAR\_HARMONICS & 31 \\
    ACOUSTIC\_BASS & 32 \\
    ELECTRIC\_BASS\_FINGER & 33 \\
    ELECTRIC\_BASS\_PICK & 34 \\
    FRETLESS\_BASS & 35 \\
    SLAP\_BASS\_1 & 36 \\
    SLAP\_BASS\_2 & 37 \\
    SYNTH\_BASS\_1 & 38 \\
    SYNTH\_BASS\_2 & 39 \\
    VIOLIN & 40 \\
    VIOLA & 41 \\
    CELLO & 42 \\
    CONTRABASS & 43 \\
    TREMOLO\_STRINGS & 44 \\
    PIZZICATO\_STRINGS & 45 \\
    ORCHESTRAL\_STRINGS & 46 \\
    TIMPANI & 47 \\
    STRING\_ENSEMBLE\_1 & 48 \\
    STRING\_ENSEMBLE\_2 & 49 \\
    SYNTH\_STRINGS\_1 & 50 \\
    SYNTH\_STRINGS\_2 & 51 \\
    CHOIR\_AAHS & 52 \\
    VOICE\_OOHS & 53 \\
    SYNTH\_VOICE & 54 \\
    ORCHESTRA\_HIT & 55 \\
    TRUMPET & 56 \\
    TROMBONE & 57 \\
    TUBA & 58 \\
    MUTED\_TRUMPET & 59 \\
    FRENCH\_HORN & 60 \\
    BRASS\_SECTION & 61 \\
    SYNTHBRASS\_1 & 62 \\
    SYNTH\_BRASS\_1 & 62 \\
    SYNTHBRASS\_2 & 63 \\
    SYNTH\_BRASS\_2 & 63 \\
    SOPRANO\_SAX & 64 \\
    ALTO\_SAX & 65 \\
    TENOR\_SAX & 66 \\
    BARITONE\_SAX & 67 \\
    OBOE & 68 \\
    ENGLISH\_HORN & 69 \\
    BASSOON & 70 \\
    CLARINET & 71 \\
    PICCOLO & 72 \\
    FLUTE & 73 \\
    RECORDER & 74 \\
    PAN\_FLUTE & 75 \\
    BLOWN\_BOTTLE & 76 \\
    SKAKUHACHI & 77 \\
    WHISTLE & 78 \\
    OCARINA & 79 \\
    LEAD\_SQUARE & 80 \\
    SQUARE & 80 \\
    LEAD\_SAWTOOTH & 81 \\
    SAWTOOTH & 81 \\
    LEAD\_CALLIOPE & 82 \\
    CALLIOPE & 82 \\
    LEAD\_CHIFF & 83 \\
    CHIFF & 83 \\
    LEAD\_CHARANG & 84 \\
    CHARANG & 84 \\
    LEAD\_VOICE & 85 \\
    VOICE & 85 \\
    LEAD\_FIFTHS & 86 \\
    FIFTHS & 86 \\
    LEAD\_BASSLEAD & 87 \\
    BASSLEAD & 87 \\
    PAD\_NEW\_AGE & 88 \\
    NEW\_AGE & 88 \\
    PAD\_WARM & 89 \\
    WARM & 89 \\
    PAD\_POLYSYNTH & 90 \\
    POLYSYNTH & 90 \\
    PAD\_CHOIR & 91 \\
    CHOIR & 91 \\
    PAD\_BOWED & 92 \\
    BOWED & 92 \\
    PAD\_METALLIC & 93 \\
    METALLIC & 93 \\
    PAD\_HALO & 94 \\
    HALO & 94 \\
    PAD\_SWEEP & 95 \\
    SWEEP & 95 \\
    FX\_RAIN & 96 \\
    RAIN & 96 \\
    FX\_SOUNDTRACK & 97 \\
    SOUNDTRACK & 97 \\
    FX\_CRYSTAL & 98 \\
    CRYSTAL & 98 \\
    FX\_ATMOSPHERE & 99 \\
    ATMOSPHERE & 99 \\
    FX\_BRIGHTNESS & 100 \\
    BRIGHTNESS & 100 \\
    FX\_GOBLINS & 101 \\
    GOBLINS & 101 \\
    FX\_ECHOES & 102 \\
    ECHOES & 102 \\
    FX\_SCI-FI & 103 \\
    SCI-FI & 103 \\
    SITAR & 104 \\
    BANJO & 105 \\
    SHAMISEN & 106 \\
    KOTO & 107 \\
    KALIMBA & 108 \\
    BAGPIPE & 109 \\
    FIDDLE & 110 \\
    SHANAI & 111 \\
    TINKLE\_BELL & 112 \\
    AGOGO & 113 \\
    STEEL\_DRUMS & 114 \\
    WOODBLOCK & 115 \\
    TAIKO\_DRUM & 116 \\
    MELODIC\_TOM & 117 \\
    SYNTH\_DRUM & 118 \\
    REVERSE\_CYMBAL & 119 \\
    GUITAR\_FRET\_NOISE & 120 \\
    BREATH\_NOISE & 121 \\
    SEASHORE & 122 \\
    BIRD\_TWEET & 123 \\
    TELEPHONE\_RING & 124 \\
    HELICOPTER & 125 \\
    APPLAUSE & 126 \\
    GUNSHOT & 127
  \end{longtable}
\end{appendices}
\end{document}
