#!/bin/bash

if [ ! -e doc ]
then
    mkdir doc
fi

for i in *.hs
do
    outputsrc="${i}_src"
    hscolour -odoc/${outputsrc}.html -anchor -css $i
done

hscolour --print-css > doc/hscolour.css
# haddock -s "%{FILE}_src.html" --source-entity="%{FILE}_src.html#line-%L" -h -o doc/ Musik.hs
haddock -s "%{FILE}_src.html" -i /usr/lib/ghc-doc/haddock/ghc/base-4.10.1.0/base.haddock -i /usr/lib/ghc-doc/haddock/ghc/containers-0.5.10.2/containers.haddock -i /usr/lib/ghc-doc/haddock/hashable-1.2.7.0/hashable.haddock -i /usr/lib/ghc-doc/haddock/ghc/array-0.5.2.0/array.haddock -i /usr/lib/ghc-doc/haddock/megaparsec-6.4.1/megaparsec.haddock -i /usr/lib/ghc-doc/haddock/unordered-containers-0.2.9.0/unordered-containers.haddock -i /usr/lib/ghc-doc/haddock/ghc/transformers-0.5.2.0/transformers.haddock -i /usr/lib/ghc-doc/haddock/text-1.2.3.0/text.haddock -i /usr/lib/ghc-doc/haddock/dlist-0.8.0.4/dlist.haddock -i ~/.cabal/share/doc/x86_64-linux-ghc-8.2.2/HCodecs-0.5/html/HCodecs.haddock -h -o doc/ Musik.hs
