{-# LANGUAGE Safe #-}
{-
  Copyright 2010–2015 Chiraag M. Nataraj
  This file is part of Musik.
  
  Musik is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

-- |Musik is a program which converts text into music.

module Main where
    
    -- Imports

    import Common
    import Constants
    import Datatypes
    import DatToMidi
    import MusikInterpreter
    import MusikParser

    import Control.Monad.State
    import Control.Arrow
    import Data.Foldable
    import Data.Maybe
    import qualified Data.Sequence as S
    import qualified Data.Text as T
    import System.Console.GetOpt
    import System.Environment
    import System.Exit
    import System.IO

    -- *Datatypes

    -- |'Flag' is an ancillary data type used by 'GetOpt'.

    data Flag = Version
              | Help
              | Verbose
              | LogFile String

    -- |'OptArgState' stores the state of different variables as
    -- optional arguments are parsed.
              
    data OptArgState =
        OptArgState
        {
          oaexit :: Bool
        , oastatus :: Int
        , oaquiet :: Bool
        , oalog :: String
        , oausage :: Bool
        }

    -- *Variables

    -- |'numRequiredArgs' is the number of arguments Musik
    -- requires to function.
                     
    numRequiredArgs :: Int
    numRequiredArgs = 1

    -- |'exitNoError' defines the exit status if there is no problem.
                      
    exitNoError :: Int
    exitNoError = 0

    -- |'exitRequiredArg' defines the exit status if the required arguments
    -- are missing.
                  
    exitRequiredArg :: Int
    exitRequiredArg = 1

    -- |'exitUnknownOption' defines the exit status if an unknown option
    -- is passed.
                      
    exitUnknownOption :: Int
    exitUnknownOption = 2

    -- *Functions

    -- |'compilerOpts' parses options and returns the
    -- parsed required and optional arguments.
                       
    compilerOpts :: [String] -> IO ([Flag], [String])
    compilerOpts argv = 
        case getOpt Permute options argv of
          (o, n, []) -> return (o, n)
          _          -> musikExit True exitUnknownOption

    -- |'exitWithoutUsage' just exits with the specified exit code.
                   
    exitWithoutUsage :: Int -> IO a
    exitWithoutUsage = uncurry
                       ((. (== 0)) . ifthenelse exitSuccess .
                        exitWith . ExitFailure) .
                       (id &&& id)

    -- |'exportWrap' handles exporting the processed data to either
    -- the specified file or a default file.
                         
    exportWrap :: (Musik a) => ExportFileType -> [String] -> a -> IO ()
    exportWrap MidiFile [] = export "out.mid"
    exportWrap MidiFile f  = (export . flip (++) ".mid" . head) f
    exportWrap a _ = (errorWithoutStackTrace . show) a

    -- |'isFileToSave' returns true if the 'Dat' is a 'FileToSave'.

    isFileToSave :: Dat -> Bool
    isFileToSave (FileToSave _) = True
    isFileToSave _ = False

    -- |'findFileToSave' returns all instances of the 'FileToSave'
    -- data constructor in the list of 'FunctionReturn'.
                     
    findFileToSave :: S.Seq FunctionReturn -> S.Seq String
    findFileToSave = fmap (fromFileToSave . fromJust) .
                     S.filter
                      (uncurry (&&) .
                       (isJust &&& isFileToSave . fromJust))

    -- |'main' does all the heavy lifting with actually reading in
    -- the Musik file, parsing it, calling 'interpret' to interpret
    -- the commands, and then doing some post-processing and exporting
    -- the result to a MIDI file.
               
    main :: IO ()
    main =
        do
          putStrLn copyright
          args <- getArgs
          (opts, reqs) <- compilerOpts args
          let s = execState
                  (mapM_ parseOptArg opts)
                  (OptArgState False exitNoError True "" False)
              q = oaquiet s
              logfile = oalog s
          parseExit reqs s
          putStrLn beginParse
          c <- parseMusikFile (head reqs)
          putStrLn endParse
          putStrLn beginInterpret
          let (ret,fstate) = (interpret . S.fromList . zip [1..]) c
              myProcessed = process ret
              fileToSave = findFileToSave ret
          outputMessages logfile q ((toList . fmessage) fstate)
          putStrLn endInterpret
          putStrLn beginExport
          exportWrap MidiFile (toList fileToSave) (myProcessed :: MidiWrapper)
          putStrLn endExport

    -- |'message' is used to print verbose output if -v is provided as a
    -- command-line argument and print to a log file if -l is provided
    -- as a command-line argument.
                        
    message :: Bool -> T.Text -> Maybe Handle -> IO ()
    -- message True m f = maybe (putStr "") (\f -> (hPutStrLn f . T.unpack) m) f
    -- message False m f = maybe ((putStrLn . T.unpack) m) (\f -> (putStrLn . T.unpack) m >> (hPutStrLn f . T.unpack) m) f
    message True  = maybe (putStr "") .
                    flip ((. T.unpack) . hPutStrLn)
    message False = uncurry
                    (flip
                     (maybe . putStrLn . T.unpack) .
                     flip
                     ((uncurry (>>) .) .
                      ((putStrLn . T.unpack) &&&) .
                      (. T.unpack) .
                      hPutStrLn)) .
                    (id &&& id)
                                   
    -- |'musikExit' either calls 'usage' and then exits or just exits.
                         
    musikExit :: Bool -> Int -> IO a
    musikExit = ifthenelse ((usage >>) . exitWithoutUsage) exitWithoutUsage

    -- |'options' details the command-line options that are possible.
                                                 
    options :: [OptDescr Flag]
    options = [Option
               ['h']
               ["help"]
               (NoArg Help)
               "show this help"
              ,Option
               ['l']
               ["log"]
               (ReqArg LogFile "FILE")
               "write log file to FILE"
              ,Option
               ['v']
               ["verbose"]
               (NoArg Verbose)
               "Debugging output"
              ,Option
               ['V']
               ["version"]
               (NoArg Version)
               "show version number"]

    -- |'outputMessages' outputs debug messages to the log file and/or stdout.
              
    outputMessages :: String -> Bool -> [T.Text] -> IO ()
    outputMessages =
        uncurry
        (ifthenelse
         (\_ q -> mapM_ (flip (message q) Nothing))
         (\l q ->
          (openFile l WriteMode >>=) .
          (uncurry (>>) .) .
          (&&& hClose) .
          (. (flip (message q) . Just)) . forM_)) .
        (null &&& id)
                        

    -- |'parseExit' checks for the various conditions under which
    -- Musik should exit.
                 
    parseExit :: [String] -> OptArgState -> IO ()
    parseExit reqs s | oaexit s = musikExit (oausage s) (oastatus s)
                     | length reqs /= numRequiredArgs = musikExit True exitRequiredArg
                     | otherwise = return ()

    -- |'parseOptArg' parses the optional arguments defined in 'options'.

    parseOptArg :: Flag -> State OptArgState ()
    parseOptArg x =
        do
          curState <- get
          case x of
            Version -> put curState { oaexit = True
                                    , oastatus = exitNoError
                                    , oausage = False }
            Help -> put curState { oaexit = True
                                 , oastatus = exitNoError
                                 , oausage = True }
            Verbose -> put curState { oaquiet = False }
            (LogFile l) -> put curState { oalog = l }

    -- |'usage' prints usage info.
              
    usage :: IO ()
    usage = getProgName >>= (putStrLn . flip usageInfo options . flip (++) " [OPTIONS...] file" . (++) "Usage: ")
