{-# LANGUAGE Safe #-}
{-
  Copyright 2010–2015 Chiraag M. Nataraj
  This file is part of Musik.
  
  Musik is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

-- |Common contains functions which are used by more than one module
-- which do not otherwise depend on each other.

module Common where
    import Datatypes

    -- |'defaultDuration' specifies the the return value for any 'Dat'
    -- that does not have a real duration.
        
    defaultDuration :: Double
    defaultDuration = 0.0
        
    -- * Functions

    -- |'calcDuration' calculates the duration of any 'Dat'.
        
    calcDuration :: Dat -> Double
    calcDuration (Note _ d) = d
    calcDuration (ListDat x) = foldr ((+) . calcDuration) 0 x
    calcDuration (Simul x) = foldr (max . calcDuration) defaultDuration x
    calcDuration (Repeat i x) = fromIntegral i * calcDuration x
    calcDuration (Staccato x) = calcDuration x
    calcDuration (Legato x) = calcDuration x
    calcDuration (Tuplet x) = calcDuration x
    calcDuration (Chord _ x) = calcDuration x
    calcDuration _ = defaultDuration

    -- |'ifthenelse' is merely a functional version of the
    -- if-then-else construct
    
    ifthenelse :: t -> t -> Bool -> t
    ifthenelse f _ True =f
    ifthenelse _ g _ = g
