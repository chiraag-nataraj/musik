{-# LANGUAGE Safe #-}
{-
  Copyright 2010–2015 Chiraag M. Nataraj
  This file is part of Musik.
  
  Musik is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

-- |Datatypes contains the datatype definitions used throughout Musik

module Datatypes where

    import Control.Monad.State as CMS
    import Data.Hashable
    import qualified Data.HashMap.Lazy as HM
    import qualified Data.Text as T
    import qualified Data.Sequence as S

    -- *Type Classes

    -- |'Musik' is a class that implements the 'process'
    -- and 'export' functions for a given datatype.

    class Musik a where
                 -- |'process' turns a list of 'FunctionReturn' into an
                 -- instance of the paramterized datatype.
                 process :: S.Seq FunctionReturn -> a
                 -- |'export' writes the result of 'process' to the given file.
                 export :: String -> a -> IO ()

    -- *Datatypes used for parsing

    -- |'Atom' is an intermediate datatype used in parsing.

    data Atom = NameA { fromAtom :: !String }
              | StuffA { fromAtom :: !String } deriving (Eq,Show)

    -- |Simple hash implementation for 'Atom'.

    instance Hashable Atom where
            hashWithSalt i = (+) i . hash . fromAtom

    -- |'ChordHelp' is an intermediate datatype used to interpret
    -- chord commands.

    newtype ChordHelp = ChordHelp [Int]

    -- |'Command' is an intermediate datatype used in parsing.

    data Command = AtomS !Atom
                 | ListS ![Command] deriving (Eq,Show)

    -- |'NoteHelp' is an intermediate datatype used in parsing.

    data NoteHelp = NoteHelp !String !String !Int !String
                  | BeatHelp !String !Double deriving Show

    -- *Datatypes used in input validation

    -- |'Arguments' is a datatype used to delineate how many
    -- arguments each command takes.

    data Arguments = Exactly !Int
                   | Between !Int !Int

    -- |Show implementation for 'Arguments' (mainly used for error messages).

    instance Show Arguments where
            show (Exactly i) = "exactly " ++ show i
            show (Between i j) = "between " ++ show i ++ " and " ++ show j

    -- |'CommandCheck' is a convenience type that allows the collection
    -- of multiple error messages during verification of valid commands.

    type CommandCheck = Either String Bool

    -- *Datatypes used to implement commands

    -- |'CommandType' is used internally to record the type of command
    -- present in a 'FunctionArg'

    data CommandType = C
                     | NB
                     | LC

    -- |'Dat' is the main datatype. It has several constructors used to
    -- delineate what type of data is being held. This makes it easy to
    -- convert the parsed AST into something useful.

    data Dat = Emp
             | SaveOnly
             | ListDat { listDats :: ![Dat] }
             | Note { noteVal :: !Int, noteDuration :: !Double }
             | Instrument !Int !Int
             | Tempo !Int
             | Repeat !Int !Dat
             | Staccato !Dat
             | Legato !Dat
             | Chord !Dat !Dat
             | Simul ![Dat]
             | Tuplet !Dat
             | Soundbank !String
             | FileToSave { fromFileToSave :: !String }
             | Beat !Dat !Dat deriving (Eq,Show)

    -- |'ExportFileType' denotes the type of file being exported.

    data ExportFileType = MidiFile | WavFile

    -- |Show implementation of 'ExportFileType' (used to display
    -- errors)

    instance Show ExportFileType where
                 show MidiFile = "Export to midi not implemented."
                 show WavFile = "Export to wav not implemented."

    -- |'FunctionArgs' is the input type of all functions that implement
    -- commands.

    data FunctionArgs = FunctionArgs { lineno :: !Int, command :: Command }

    -- |'FunctionState' stores the state of various variables as commands
    -- are evaluated.

    data FunctionState =
        FunctionState
        {fscale :: Scale
        ,fsaved :: Saved
        ,finsts :: Insts
        ,fbeat :: Beats
        ,fmessage :: S.Seq T.Text} deriving (Eq,Show)

    -- |'FunctionReturn' is the return type of all functions that
    -- implement commands.

    type FunctionReturn = Maybe Dat

    -- |'FunctionType' is the type of every function that implements a specific
    -- command.

    type FunctionType = FunctionArgs -> CMS.State FunctionState FunctionReturn

    -- |'ScaleType' records the type of scale being generated.

    data ScaleType = Major | Minor | Æolian | Locrian | Dorian
                   | Phrygian | Lydian | Mixolydian

    -- *Miscellaneous datatypes

    -- |'Beats' is the datatype of the global 'drums' variable.

    type Beats = HM.HashMap String Int

    -- |'Insts' is the datatype of the global 'insts' variable.

    type Insts = HM.HashMap String Int

    -- |'Saved' is the datatype of the variable which stores the sections
    -- created by the user.

    type Saved = HM.HashMap String Dat

    -- |'Scale' is the datatype of the global 'notes' variable.

    type Scale = S.Seq Int

    -- |'Language' is the datatype which selects the language.
    -- Currently, English only, but I hope to localize.

    data Language = English deriving (Show,Eq)

    instance Hashable Language where
                 hashWithSalt i = (+) i . hash . show

    -- |'PrintLogType' is the function type for implementing localized
    -- logging.
    type PrintLogType = [String] -> String
