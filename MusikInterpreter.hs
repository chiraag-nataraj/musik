{-# LANGUAGE Trustworthy,LambdaCase #-}
{-
  Copyright 2010–2015 Chiraag M. Nataraj
  This file is part of Musik.
  
  Musik is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

-- |MusikInterpreter is a library that interprets and
-- expands Musik commands to actual notes.

module MusikInterpreter
    (
     -- *Functions                    
     interpret
    )
    where

    -- Imports

    import Common
    import Constants
    import Datatypes
    import MusikParser

    import Control.Applicative
    import Control.Arrow
    import Control.Monad.State
    import Data.Char
    import qualified Data.Foldable as F
    import qualified Data.HashMap.Lazy as HM
    import Data.Maybe
    import qualified Data.Sequence as S
    import qualified Data.Text as T
    import System.IO.Unsafe
    import Text.Megaparsec

    -- Variables

    -- 'validCommands' stores a map of command names to a tuple.
    -- The first element of the tuple is the number of arguments
    -- expected (an instance of 'Arguments'). The second element
    -- of the tuple is the function to call when that command is
    -- found (of type 'FunctionType').

    validCommands :: HM.HashMap Atom (Arguments, FunctionType, HM.HashMap Language PrintLogType)
    validCommands = HM.fromList
                     [(NameA "addBeatMap"
                      ,(Exactly 2
                       ,runAddBeat
                       ,HM.fromList
                        [(English
                         ,\[name,note] ->
                          "Adding beat mapping from " ++ name ++ " to " ++ note)]))
                     ,(NameA "addInstrument"
                      ,(Exactly 2
                       ,addInstrument
                       ,HM.fromList
                        [(English
                         ,\[number,name] ->
                          "Adding instrument mapping from " ++ number ++ " to " ++ name)]))
                     ,(NameA "beat"
                      ,(Exactly 2
                       ,runBeat
                       ,HM.empty))
                     ,(NameA "chord"
                      ,(Exactly 2
                       ,changeChord
                       ,HM.fromList
                        [(English
                         ,\[chord] ->
                          "Changing chord to " ++ chord)]))
                     ,(NameA "file"
                      ,(Exactly 1
                       ,runNoPromptFile
                       ,HM.fromList
                        [(English
                         ,\[file] ->
                          "Saving to: " ++ file)]))
                     ,(NameA "import"
                      ,(Exactly 1
                       ,runImport
                       ,HM.fromList
                        [(English
                         ,\[file] ->
                             "Importing " ++ file)]))
                     ,(NameA "instrument"
                      ,(Between 1 2
                       ,changeInstrument
                       ,HM.fromList
                        [(English
                         ,\[inst,voice,err] ->
                          case err of
                            "false" -> "Changing instrument to " ++ inst ++ " for voice " ++ voice
                            _ -> "Warning: instrument " ++ inst ++ " not found - restoring default"
                         )]))
                     ,(NameA "legato"
                      ,(Exactly 1
                       ,makeLegato
                       ,HM.empty))
                     ,(NameA "mode"
                      ,(Exactly 1
                       ,changeMode
                       ,HM.fromList
                        [(English
                         ,\[scaleArg] ->
                          "Changing mode to " ++ scaleArg)]))
                     ,(NameA "newSection"
                      ,(Exactly 2
                       ,runNewSection
                       ,HM.empty))
                     ,(NameA "noPromptFile"
                      ,(Exactly 1
                       ,runNoPromptFile
                       ,HM.empty))
                     ,(NameA "repeat"
                      ,(Exactly 2
                       ,runRepeat
                       ,HM.empty))
                     ,(NameA "saveOnly"
                      ,(Exactly 0
                       ,runSaveOnly
                       ,HM.fromList
                        [(English
                         ,\_ ->
                          "Warning: Save only mode is deprecated! \
                          \Musik now exports to a midi file.")]))
                     ,(NameA "scale"
                      ,(Exactly 1
                       ,changeMode
                       ,HM.empty))
                     ,(NameA "section"
                      ,(Exactly 1
                       ,runSection
                       ,HM.empty))
                     ,(NameA "simul"
                      ,(Between 1 14
                       ,runSimul
                       ,HM.empty))
                     ,(NameA "slur"
                      ,(Exactly 1
                       ,makeLegato
                       ,HM.empty))
                     ,(NameA "soundbank"
                      ,(Exactly 1
                       ,runSoundbank
                       ,HM.fromList
                        [(English
                         ,\_ ->
                             "Warning: soundbank is deprecated! Musik \
                             \now exports to a midi file. Use your midi \
                             \software to select the soundbank.")]))
                     ,(NameA "staccato"
                      ,(Exactly 1
                       ,makeStaccato
                       ,HM.empty))
                     ,(NameA "tempo"
                      ,(Exactly 1
                       ,changeTempo
                       ,HM.fromList [(English,\[tmp] -> "Changing tempo to " ++ tmp)]))
                     ,(NameA "transpose"
                      ,(Exactly 1
                       ,changeTranspose
                       ,HM.fromList [(English,\[amt] -> "Transposing by " ++ amt)]))
                     ,(NameA "tuplet"
                      ,(Exactly 1
                       ,runTuplet
                       ,HM.empty))]

    highestNote :: Num a => a
    highestNote = 72

    octave :: Num a => a
    octave = 12

    restPitch :: Num a => a
    restPitch = 0

    defaultChannel :: String
    defaultChannel = "0"

    defaultVoice :: Int
    defaultVoice = 0

    curLanguage :: Language
    curLanguage = English

    -- Functions

    -- 'convertToPureCommand' runs __unsafePerformIO__ to extract the
    -- list of commands from inside the IO monad. This function is only
    -- used in one place (the implementation of the \import command)
    -- and there should not be any side effects (since the IO is a
    -- residual of the fact that the data came from parsing a file).
    -- __Don't use this function - it's only meant for internal use.__
                        
    convertToPureCommand :: IO [Command] -> [Command]
    convertToPureCommand = unsafePerformIO

    getLogMessage :: String -> [String] -> T.Text
    getLogMessage = (T.pack .) . (flip (HM.!) curLanguage . (\(_,_,x) -> x) . (HM.!) validCommands . NameA)
    
    -- 'foldCCheck' combines all of the individual 'CommandCheck's into one
    -- 'CommandCheck' that consolidates all of the individual errors or
    -- simply returns 'True'.
                    
    foldCCheck :: S.Seq CommandCheck -> CommandCheck
    foldCCheck =
        foldr
        (\x y ->
         case (x,y) of
           (Right _, Right _) -> Right True
           (Left err1, Left err2) -> Left (err1 ++ err2)
           (z@(Left _), _) -> z
           (_, z@(Left _)) -> z)
        (Right True)

    -- 'checkCommandAndArgs' checks to see if a given 'Command' is a valid
    -- command (by making use of 'validCommands') and if it has a valid number
    -- of arguments. If there is a 'Command' it does not recognize, it throws
    -- an error.

    checkCommandAndArgs :: Int -> Command -> CommandCheck
    checkCommandAndArgs line (ListS l@(AtomS commandName@(NameA c):rest)) =
        if HM.member commandName validCommands
        then
            let (n,_,_) = (HM.!) validCommands commandName
                numArgs = case n of
                            (Exactly i) -> (i ==)
                            (Between i j) -> \x -> (x >= i) && (x <= j)
            in
              if numArgs (length l - 1)
              then
                  foldCCheck (fmap (checkCommandAndArgs line) (S.fromList rest))
              else
                  Left ("Invalid number of arguments for command "
                        ++ c
                        ++ " on line "
                        ++ show line
                        ++ " - expected "
                        ++ show n
                        ++ " and got "
                        ++ show (Exactly (length l - 1))
                        ++ ". ")
        else
            Left ("Invalid command on line " ++ show l ++ " : " ++ c ++ ". ")
    checkCommandAndArgs i (ListS l) = foldCCheck (fmap (checkCommandAndArgs i) (S.fromList l))
    checkCommandAndArgs _ _ = Right True

    -- 'checkCommandsAndArgs' runs 'checkCommandAndArgs' on every 'Command'
    -- in its input list.
                              
    checkCommandsAndArgs :: S.Seq (Int,Command) -> CommandCheck
    checkCommandsAndArgs = foldCCheck . fmap (uncurry checkCommandAndArgs)

    -- 'getScale' is just an easier way to access 'notes'.

    getScale :: Scale
    getScale = notes

    -- 'catchIllegalCall' is a simple error handler.
                 
    catchIllegalCall :: Command -> a
    catchIllegalCall = errorWithoutStackTrace . (++) "Unexpected: " . show

    catchCommandError :: Int -> String -> a
    catchCommandError = (.) (errorWithoutStackTrace . ("Line " ++)) .
                        (. (": " ++)) . (++) . show

    -- 'checkFunctionCommand' checks whether the command in f
    -- has the name x.
                         
    checkFunctionCommand :: Command -> String -> Bool
    checkFunctionCommand cm x =
        case cm of
          (ListS (c:_)) ->
              c == AtomS (NameA x)
          _ -> error "Not a command"

    -- 'extractFunctionCommandArgs' extracts the arguments
    -- of the command in f.
            
    extractFunctionCommandArgs :: Command -> [Command]
    extractFunctionCommandArgs com =
        case com of
          (ListS (_:c)) -> c
          _ -> error "Not a command"

    -- 'joinMessages' combines the messages from a list of
    -- 'FunctionState'.
          
    joinMessages :: [FunctionState] -> S.Seq T.Text
    joinMessages = foldr ((S.><) . fmessage) S.empty

    -- 'normalizeScale' converts and arbitrary scale into
    -- notes between 60 and 71 (inclusive). This is exclusively
    -- used by 'genScaleHelp' so it expects input between 60 and
    -- 83 (which is why it just subtracts 12 instead of taking
    -- the mod and adding to 60).

    normalizeScale :: (Num a,Ord a,Functor f) => f a -> f a
    normalizeScale =
        (fmap . flip (.) (id &&& id) . uncurry . flip (.) (id &&& id) . uncurry)
        (flip (.) (>= highestNote) . flip . ifthenelse . flip (-) octave)

    -- 'genScaleHelp' generates a scale with the specified
    -- increments (specified as sections).
            
    genScaleHelp :: (Num b,Ord b) => a -> S.Seq (a -> b) -> S.Seq b
    genScaleHelp = ((S.sort . normalizeScale . (S.<|) restPitch) .) .
                    flip (<*>) . S.singleton

    -- 'genScale' creates the appropriate type of scale
    -- starting at the given starting value.
                                    
    genScale :: (Num a,Ord a) => ScaleType -> a -> S.Seq a
    genScale Major = (flip genScaleHelp . S.fromList) [(+0),(+2),(+4),(+5),(+7),(+9),(+11)]
    genScale Minor = (flip genScaleHelp . S.fromList) [(+0),(+2),(+3),(+5),(+7),(+8),(+11)]
    genScale Æolian = (flip genScaleHelp . S.fromList) [(+0),(+2),(+3),(+5),(+7),(+8),(+10)]
    genScale Locrian = (flip genScaleHelp . S.fromList) [(+0),(+1),(+3),(+5),(+6),(+8),(+10)]
    genScale Dorian = (flip genScaleHelp . S.fromList) [(+0),(+2),(+3),(+5),(+7),(+9),(+10)]
    genScale Phrygian = (flip genScaleHelp . S.fromList) [(+0),(+1),(+3),(+5),(+7),(+8),(+10)]
    genScale Lydian = (flip genScaleHelp . S.fromList) [(+0),(+2),(+4),(+6),(+7),(+9),(+11)]
    genScale Mixolydian = (flip genScaleHelp . S.fromList) [(+0),(+2),(+4),(+5),(+7),(+9),(+10)]

    inc :: Int -> Int
    inc = (+1)

    dec :: Int -> Int
    dec = (+(-1))
                          
    -- 'changePitch' takes a pitch and applies the given sequence
    -- of sharps and flats to it.
                          
    changePitch :: Int -> String -> Int
    changePitch = flip (.)
                      (map (\x -> if x == '#' then inc else dec)) .
                  foldl (flip ($))

    -- 'changeMode' implements the "scale" and "mode" commands.

    changeMode :: FunctionType
    changeMode f
        | (checkFunctionCommand . command) f "mode"
          ||
          (checkFunctionCommand . command) f "scale" =
            do
              fstate <- get
              case (extractFunctionCommandArgs . command) f of
                [ListS [AtomS (StuffA scaleArg)]] ->
                    let p = parse parseMode "" scaleArg
                        fm = fmessage
                             fstate
                             S.|>
                             getLogMessage "mode" [scaleArg]
                    in
                      either
                       (\err -> catchCommandError (lineno f) ("Unexpected: " ++ show err))
                       (\(nh,m) ->
                        case nh of
                          (NoteHelp pitch acc _ _) ->
                              let base = (HM.!) stringToMidi pitch
                                  note = changePitch base acc
                              in
                                put fstate { fmessage = fm
                                           , fscale = genScale m note }
                          _ -> (catchIllegalCall . command) f)
                       p
                       >> return Nothing
                _ -> (catchIllegalCall . command) f
        | otherwise = (catchIllegalCall . command) f

    -- 'changeTranspose' implements the "transpose" command.

    changeTranspose :: FunctionType
    changeTranspose f
        | (checkFunctionCommand . command) f "transpose" =
            do
              fstate <- get
              case (extractFunctionCommandArgs . command) f of
                [ListS [AtomS (StuffA amt)]] ->
                    let val = read amt :: Int
                        s = fscale fstate
                        transvals = (tail . F.toList) s
                        newscale = restPitch : map (+val) transvals
                    in
                      put fstate {
                                fscale = S.fromList newscale,
                                fmessage = fmessage fstate
                                           S.|>
                                           getLogMessage "transpose" [amt]
                              }
                      >> return Nothing
                _ -> (catchIllegalCall . command) f
        | otherwise = (catchIllegalCall . command) f

    -- 'changeTempo' implements the "tempo" command.

    changeTempo :: FunctionType
    changeTempo f
        | (checkFunctionCommand . command) f "tempo" =
            do
              fstate <- get
              case (extractFunctionCommandArgs . command) f of
                [ListS [AtomS (StuffA tmp)]] ->
                    let tempo = read tmp :: Int
                        fm = fmessage fstate
                    in
                      put fstate
                              {
                                fmessage =
                                    fm
                                    S.|>
                                    getLogMessage "tempo" [tmp]
                              }
                      >> (return . Just . Tempo) tempo
                _ -> (catchIllegalCall . command) f
        | otherwise = (catchIllegalCall . command) f

    -- 'extractStuff' extracts the voice and instrument from the arguments
    -- to the "instrument" command.
                      
    extractStuff :: [Command] -> (String,String)
    extractStuff [ListS [AtomS (StuffA inst)]] = (defaultChannel,inst)
    extractStuff [ListS [AtomS (StuffA voice)],
                 ListS [AtomS (StuffA inst)]] = (voice,inst)
    extractStuff _ = ("","")
                      
    -- 'changeInstrument' implements the "instrument" command.

    changeInstrument :: FunctionType
    changeInstrument f
        | (checkFunctionCommand . command) f "instrument" =
            do
              fstate <- get
              let stuff = (extractFunctionCommandArgs . command) f
                  fm = fmessage fstate
                  fi = finsts fstate
                  (voice,inst) = extractStuff stuff
                  mess = getLogMessage "instrument" [inst, voice, "false"]
                  mess2 = getLogMessage "instrument" [inst, voice, "true"]
              case (voice,inst) of
                ("","") -> (catchIllegalCall . command) f
                _ ->
                    do
                      let ci = HM.lookupDefault defaultVoice (map toUpper inst) fi
                      ifthenelse
                        (put fstate { fmessage = fm S.|> mess })
                        (put fstate { fmessage = fm S.|> mess S.|> mess2 })
                        (HM.member (map toUpper inst) fi)
                      (return . Just . Instrument (read voice :: Int)) ci
        | otherwise = (catchIllegalCall . command) f

    -- 'addInstrument' implements the "addInstrument" command.

    addInstrument :: FunctionType
    addInstrument f
        | (checkFunctionCommand . command) f "addInstrument" =
            do
              fstate <- get
              case (extractFunctionCommandArgs . command) f of
                [ListS [AtomS (StuffA name)],ListS [AtomS (StuffA number)]] ->
                    let i = finsts fstate
                        fm = fmessage fstate
                        em = getLogMessage "addInstrument" [name,show number]
                    in
                      put fstate
                              {
                                fmessage = fm S.|> em,
                                finsts =
                                    HM.insert
                                         (map toUpper name)
                                         (read number :: Int)
                                         i
                              }
                      >> return Nothing
                _ -> (catchIllegalCall . command) f
        | otherwise = (catchIllegalCall . command) f
          
    -- 'runNewSection' implements the "newSection" command.

    runNewSection :: FunctionType
    runNewSection f
        | (checkFunctionCommand . command) f "newSection" =
            do
              fstate <- get
              case (extractFunctionCommandArgs . command) f of
                [ListS [AtomS (StuffA name)],commands] ->
                    let c = fsaved fstate
                        (a, estate) = runState
                                       (runCommand f { command = commands })
                                       fstate { fmessage = S.empty }
                        fm = fmessage fstate
                        cm = T.pack ("Storing section " ++ name)
                        em = fmessage estate
                    in
                      put fstate { fmessage = (fm S.|> cm) S.>< em
                                 , fsaved = HM.insert name (fromMaybe Emp a) c }
                      >> return Nothing
                _ -> (catchIllegalCall . command) f
        | otherwise = (catchIllegalCall . command) f

    -- 'runSection' implements the "section" command.

    runSection :: FunctionType
    runSection f
        | (checkFunctionCommand . command) f "section" =
            do
              fstate <- get
              case (extractFunctionCommandArgs . command) f of
                [ListS [AtomS (StuffA name)]] ->
                  let c = fsaved fstate
                      fm = fmessage fstate
                      mess1 = T.pack ("Recalling section " ++ name)
                      mess2 = T.pack ("Warning: no such section: " ++ name)
                      memb = HM.lookup name c
                      mess = maybe (T.append mess1 mess2) (const mess1) memb
                  in
                    put fstate { fmessage = fm S.|> mess }
                    >> return (Just (fromMaybe Emp memb))
                _ -> (catchIllegalCall . command) f
        | otherwise = (catchIllegalCall . command) f

    -- 'runRepeat' implements the "repeat" command.

    runRepeat :: FunctionType
    runRepeat f
        | (checkFunctionCommand . command) f "repeat" =
            do
              fstate <- get
              case (extractFunctionCommandArgs . command) f of
                [ListS [AtomS (StuffA t)],cs] ->
                    let (a, estate) = runState
                                       (runCommand f { command = cs })
                                       fstate { fmessage = S.empty }
                        fm = fmessage fstate
                        cm = T.pack ("Repeating expression " ++ t ++ " times")
                        em = fmessage estate
                    in
                      put fstate { fmessage = (fm S.|> cm) S.>< em }
                      >> (return . fmap (Repeat (read t :: Int))) a
                _ -> (catchIllegalCall . command) f
        | otherwise = (catchIllegalCall . command) f

    -- 'makeStaccato' implements the "staccato" command.

    makeStaccato :: FunctionType
    makeStaccato f
        | (checkFunctionCommand . command) f "staccato" =
            do
              fstate <- get
              case (extractFunctionCommandArgs . command) f of
                [commands] ->
                    let (a, estate) =
                            runState
                             (runCommand f { command = commands })
                             fstate { fmessage = S.empty }
                        sfm = fmessage fstate
                        sem = fmessage estate
                    in
                      put fstate { fmessage = sfm S.>< sem }
                      >> (return . fmap Staccato) a
                _ -> (catchIllegalCall . command) f
        | otherwise = (catchIllegalCall . command) f

    -- 'makeLegato' implements the "slur" and "legato" commands.

    makeLegato :: FunctionType
    makeLegato f
        | (checkFunctionCommand . command) f "slur"
          ||
          (checkFunctionCommand . command) f "legato" =
              do
                fstate <- get
                case (extractFunctionCommandArgs . command) f of
                  [commands] ->
                      let (a,estate) =
                              runState
                               (runCommand f { command = commands })
                               fstate { fmessage = S.empty }
                          lfm = fmessage fstate
                          lem = fmessage estate
                      in
                        put fstate { fmessage = lfm S.>< lem }
                        >> (return . fmap Legato) a
                  _ -> (catchIllegalCall . command) f
        | otherwise = (catchIllegalCall . command) f

    -- 'changeChord' implements the "chord" command.

    changeChord :: FunctionType
    changeChord f
        | (checkFunctionCommand . command) f "chord" =
            do
              fstate <- get
              case (extractFunctionCommandArgs . command) f of
                [ListS [AtomS (StuffA chord)],commands] ->
                    let s = fscale fstate
                        (a,estate) =
                            runState
                             (runCommand f { command = commands })
                             fstate { fmessage = S.empty }
                        fm = fmessage fstate
                        cm = getLogMessage "chord" [chord]
                        em = fmessage estate
                        parsedChord = parseChord s chord
                    in
                      put fstate { fmessage = (fm S.|> cm) S.>< em }
                      >>
                      maybe
                       (return Nothing)
                       (let (ChordHelp ch) = parsedChord
                        in
                          return .
                          Just .
                          uncurry
                           (Chord . ListDat .
                            flip (map . flip Note . calcDuration) ch) .
                          (id &&& id))
                       a
                _ -> (catchIllegalCall . command) f
        | otherwise = (catchIllegalCall . command) f

    -- 'extractNotes' is a helper function for 'runTuplet'
    -- which extracts all notes out of a 'Dat'.

    extractNotes :: Dat -> [Dat]
    extractNotes a@(Note _ _) = [a]
    extractNotes (ListDat l) = concatMap extractNotes l
    extractNotes (Repeat i x) = (concat . replicate i . extractNotes) x
    extractNotes (Staccato s) = extractNotes s
    extractNotes (Legato l) = extractNotes l
    extractNotes (Chord _ x) = extractNotes x
    extractNotes _ = [Emp]

    -- 'changeTuplet' is a helper function for 'runTuplet'
    -- which scales the duration of each element of 'Dat' by 1 / len.

    changeTuplet :: Dat -> Int -> Dat
    changeTuplet (Note i d) = Note i . (/) d . fromIntegral
    changeTuplet (ListDat l) = ListDat . flip map l . flip changeTuplet
    changeTuplet (Repeat i x) = Repeat i . changeTuplet x
    changeTuplet (Staccato l) = Staccato . changeTuplet l
    changeTuplet (Legato l) = Legato . changeTuplet l
    changeTuplet (Chord c x) = uncurry Chord . (changeTuplet c &&& changeTuplet x)
    changeTuplet x = const x

    -- 'runTuplet' implements the "tuplet" command.

    runTuplet :: FunctionType
    runTuplet f
        | (checkFunctionCommand . command) f "tuplet" =
            do
              fstate <- get
              case (extractFunctionCommandArgs . command) f of
                [tup] ->
                    let (a, estate) = runState
                                       (runCommand f { command = tup })
                                       fstate { fmessage = S.empty }
                        fm = fmessage fstate
                        em = fmessage estate
                    in
                      put fstate { fmessage = fm S.>< em }
                      >>
                      maybe
                       (return Nothing)
                       (\lst ->
                        let tmp = (filter (/= Emp) .
                                   concatMap extractNotes .
                                   listDats) lst
                            cond = (uncurry (all . (==)) . (head &&& tail) . map noteDuration) tmp
                        in
                          ifthenelse
                          (catchCommandError
                           (lineno f)
                           "Unexpected: All notes in a tuplet should have the same duration")
                          ((return . Just . Tuplet . changeTuplet lst . length) tmp)
                          (not cond))
                       a
                _ -> (catchIllegalCall . command) f
        | otherwise = (catchIllegalCall . command) f

    -- 'runSimul' implements the "simul" command.

    runSimul :: FunctionType
    runSimul f
        | (checkFunctionCommand . command) f "simul" =
            do
              fstate <- get
              let xs = (extractFunctionCommandArgs . command) f
                  (a,s) = unzip $ map
                          (\x ->
                           runState
                           (runCommand f { command = x })
                           fstate { fmessage = S.empty })
                          xs
                  fm = fmessage fstate
              put fstate { fmessage = fm S.>< joinMessages s }
              ifthenelse
                (return Nothing)
                ((return . Just . Simul . catMaybes) a)
                (all isNothing a)
        | otherwise = (catchIllegalCall . command) f

    -- 'runSaveOnly' prints a warning that the "saveOnly" command is deprecated.

    runSaveOnly :: FunctionType
    runSaveOnly f
        | (checkFunctionCommand . command) f "saveOnly" =
            modify
            (\g ->
             g { fmessage =
                     fmessage g
                     S.|>
                     getLogMessage "saveOnly" [] })
            >> return Nothing
        | otherwise = (catchIllegalCall . command) f

    -- 'runNoPromptFile' implements the "noPromptFile" and "file" commands.

    runNoPromptFile :: FunctionType
    runNoPromptFile f
        | (checkFunctionCommand . command) f "noPromptFile"
          ||
          (checkFunctionCommand . command) f "file" =
              case (extractFunctionCommandArgs . command) f of
                [ListS [AtomS (StuffA file)]] ->
                    modify
                    (\g ->
                     g { fmessage =
                             fmessage g
                             S.|>
                             getLogMessage "file" [file] })
                    >> (return . Just . FileToSave) file
                _ -> (catchIllegalCall . command) f
        | otherwise = (catchIllegalCall . command) f

    -- 'runSoundbank' prints a warning that the "soundbank" command is
    -- deprecated.

    runSoundbank :: FunctionType
    runSoundbank f
        | (checkFunctionCommand . command) f "soundbank" =
            modify
            (\g ->
             g { fmessage =
                     fmessage g
                     S.|>
                     getLogMessage "soundbank" [] })
            >> return Nothing
        | otherwise = (catchIllegalCall . command) f
        
    -- 'runImport' implements the "import" command.

    runImport :: FunctionType
    runImport f
        | (checkFunctionCommand . command) f "import" =
            do
              fstate <- get
              case (extractFunctionCommandArgs . command) f of
                [ListS [AtomS (StuffA file)]] ->
                    let c = convertToPureCommand (parseMusikFile file)
                        (ret,_) = (interpret . S.fromList . zip [1..]) c
                    in
                      put fstate { fmessage =
                                       fmessage fstate
                                       S.|>
                                       getLogMessage "import" [file] }
                      >> (return . Just . ListDat . catMaybes . F.toList) ret
                _ -> (catchIllegalCall . command) f
        | otherwise = (catchIllegalCall . command) f

    -- 'runBeat' implements the "beat" command.
                      
    runBeat :: FunctionType
    runBeat f
        | (checkFunctionCommand . command) f "beat" =
            do
              fstate <- get
              case (extractFunctionCommandArgs . command) f of
                [b@(ListS _), bnotes] ->
                    let (a, e1state) = runState
                                        (runCommand f { command = b })
                                        fstate { fmessage = S.empty }
                        (n, e2state) = runState
                                        (runCommand f { command = bnotes })
                                        fstate { fmessage = S.empty }
                        fm = fmessage fstate
                        e1m = fmessage e1state
                        e2m = fmessage e2state
                    in
                      put fstate { fmessage = (fm S.>< e1m) S.>< e2m }
                      >> return (liftM2 Beat a n)
                _ -> (catchIllegalCall . command) f
        | otherwise = (catchIllegalCall . command) f

    -- 'runAddBeat' implments the "addBeatMap" command.

    runAddBeat :: FunctionType
    runAddBeat f
        | (checkFunctionCommand . command) f "addBeatMap" =
            do
              fstate <- get
              case (extractFunctionCommandArgs . command) f of
                [ListS [AtomS (StuffA name)], note] ->
                    let b = fbeat fstate
                        fm = fmessage fstate
                             S.|>
                             getLogMessage "addBeatMap" [name,show note]
                        (a, estate) = runState
                                        (runCommand f { command = note })
                                        fstate { fmessage = S.empty }
                        em = fmessage estate
                        l = lineno f
                        errorm = "Only mapping beat symbols to\
                                 \ single notes is supported"
                    in
                      maybe
                        (catchCommandError l errorm)
                        (\case
                           ListDat [Note p _] ->
                                put fstate { fmessage = fm S.>< em
                                           , fbeat = HM.insert name p b }
                                >> return Nothing
                           _ -> catchCommandError l errorm)
                        a
                _ -> (catchIllegalCall . command) f
        | otherwise = (catchIllegalCall . command) f

    -- 'typeOfCommand' figures out the type of the command in f.

    typeOfCommand :: Command -> CommandType
    typeOfCommand (ListS (AtomS (NameA _):_)) = C
    typeOfCommand (AtomS (StuffA _)) = NB
    typeOfCommand (ListS _) = LC
    typeOfCommand c = catchIllegalCall c
                      
    -- 'runCommand' takes a 'FunctionArgs' and either runs the appropriate
    -- command, parses the notes, or runs 'runCommand' on every command in
    -- the 'ListS'.

    runCommand :: FunctionType
    runCommand f =
        case ((typeOfCommand . command) f,command f) of
          (C,ListS (AtomS name:_)) ->
                ((\(_,x,_) -> x) . (HM.!) validCommands) name f
          (NB,AtomS (StuffA s)) ->
              let p = parse (try parseNote <|> parseBeat) "" s
              in
                either
                (catchCommandError (lineno f) . (++) "Unexpected: " . show)
                ((get >>=) .
                 flip
                  (((return . Just) .) .
                   uncurry convertNoteHelpToNoteOrBeat .
                   (fscale &&& fbeat)))
                p
          (LC,ListS l) ->
              do
                fstate <- get
                let (mapped,estate) =
                        runState
                        (mapM
                         (\x ->
                          runCommand f { command = x })
                         l)
                        fstate
                put estate
                ifthenelse
                 (return Nothing)
                 ((return . Just . ListDat . map (fromMaybe Emp)) mapped)
                 (all isNothing mapped)
          _ -> (catchIllegalCall . command) f

    -- |'interpret' takes a list of 'Command', checks to see that each
    -- command is a known command, checks to see that each command has
    -- the right number of arguments, and then evaluates each command.

    interpret :: S.Seq (Int,Command) -> (S.Seq FunctionReturn,FunctionState)
    interpret cs =
        let
            n = getScale
            saved = HM.empty
            check = checkCommandsAndArgs cs
        in
          either
          errorWithoutStackTrace
          (const $
           runState
           (mapM (runCommand . uncurry FunctionArgs) cs)
           (FunctionState n saved instruments drums S.empty))
          check
