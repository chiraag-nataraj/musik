{-# LANGUAGE Trustworthy #-}
{-
  Copyright 2010–2015 Chiraag M. Nataraj
  This file is part of Musik.
  
  Musik is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

-- |MusikParser is a library that parses a Musik file. You should not
-- need to directly use any of the functions other than 'parseMusikFile'.

module MusikParser
    (
     -- * Functions
     convertNoteHelpToNoteOrBeat
    ,parseBeat
    ,parseChord
    ,parseMode
    ,parseMusikFile
    ,parseNote
    )
    where

    import Datatypes
    import Constants
    import Common

    import qualified Control.Monad as CM
    import Data.Char
    import qualified Data.HashMap.Lazy as HM
    import qualified Data.List as L
    import Data.List.Split (splitOn)
    import Data.Maybe
    import qualified Data.Sequence as S
    import Data.Void
    import Text.Megaparsec
    import Text.Megaparsec.Char

    type Parser = Parsec Void String
    
    -- 'parseString' parses alphanumerics and a couple of special
    -- characters @(+, ., #, -, /, _, and ^)@.
                   
    parseString :: Parser String
    parseString = some (alphaNumChar <|> oneOf "+.#-/_^")

    -- 'parseLiteralString' parses anything between two sets of quotes.

    parseLiteralString :: Parser String
    parseLiteralString = CM.liftM3
                         (const const)
                         (char '"')
                         (many (noneOf "\""))
                         (char '"')

    -- 'parseAtom' parses a string using 'parseString' and encapsulates
    -- it in a 'StuffA'.
                 
    parseAtom :: Parser Atom
    parseAtom = fmap StuffA (parseLiteralString <|> parseString)

    -- 'parseComment' parses a line starting with # and throws it away.
                
    parseComment :: Parser ()
    parseComment = CM.void (char '#' >> many (noneOf "\n") >> char '\n')

    -- 'parseWhitespace' swallows as many spaces as it can and throws them away.
                 
    parseWhitespace :: Parser ()
    parseWhitespace = CM.void (some spaceChar)

    -- 'parseSep' parses at least one comment (using 'parseComment') or
    -- whitespace (using 'parseWhitespace') and throws all of it away.

    parseSep :: Parser ()
    parseSep = CM.void (some (parseComment <|> parseWhitespace))

    parseBetween :: Char -> Char -> Parser [Command]
    parseBetween open close = CM.liftM4
                              ((const . const) const)
                              (char open)
                              (optional parseSep)
                              (parseCommandOrAtom `sepEndBy` parseSep)
                              (char close)

    -- 'parseOptionalArg' takes a string starting and ending with square
    -- brackets and parses the stuff in between using 'parseCommandOrAtom'.
               
    parseOptionalArg :: Parser [Command]
    parseOptionalArg = parseBetween '[' ']'

    -- 'parseRequiredArg' takes a string starting and ending with curly braces
    -- and parses the stuff in between using 'parseCommandOrAtom'.
                 
    parseRequiredArg :: Parser [Command]
    parseRequiredArg = parseBetween '{' '}'

    -- 'parseCommand' parses a command.

    -- Examples:
    --
    -- >>> parseTest parseCommand "\\transpose{2}"
    -- ListS [AtomS (NameA "transpose"),ListS [AtomS (StuffA "2")]]
    --
    -- >>> parseTest parseCommand "\\newSection[test]{test2}"
    -- ListS [AtomS (NameA "newSection"),
    --        ListS [AtomS (StuffA "test")],
--            ListS [AtomS (StuffA "test2")]]
    --
    -- >>> parseTest parseCommand "C4"
    -- parse error at (line 1, column 1):
    -- unexpected "C"
    -- expecting "\\"

    parseCommand :: Parser Command
    parseCommand = CM.liftM3
                   (const $
                    (.) ListS . flip (.) (map ListS . fromMaybe []) .
                    (:) . AtomS . NameA)
                   (char '\\')
                   (some letterChar)
                   (optional (many (parseOptionalArg <|> parseRequiredArg)))

    -- 'parseCommandOrAtom' parses either an atom or, if that fails,
    -- parses a command.

    -- Examples:
    --
    -- >>> parseTest parseCommandOrAtom "\\newSection[test]{test2}"
    -- ListS [AtomS (NameA "newSection"),
    --        ListS [AtomS (StuffA "test")],
    --        ListS [AtomS (StuffA "test2")]]
    --
    -- >>> parseTest parseCommandOrAtom "C4"
    -- AtomS (StuffA "C4")

    parseCommandOrAtom :: Parser Command
    parseCommandOrAtom = fmap AtomS parseAtom <|> parseCommand

    -- |'parseNote' takes a string representation of a note and separates
    -- it into the base note, accidentals, octaves, and duration and returns
    -- a 'NoteHelp'.

    -- |Examples:
    --
    -- >>> parseTest parseNote "C4"
    -- NoteHelp "C" "" 0 "4"
    --
    -- >>> parseTest parseNote "E#^^4-4.-4"
    -- NoteHelp "E" "#" 2 "4-4.-4"

    parseNote :: Parser NoteHelp
    parseNote = CM.liftM4
                ((.)
                 (. (uncurry (flip (.) length . (-) . length) .
                     L.partition ('^' ==))) .
                 NoteHelp . flip (:) [])
                upperChar
                (many (oneOf "#b"))
                (many (oneOf "^_"))
                (many (digitChar <|> oneOf ".-"))

    -- 'parseDuration' takes a string representation of a duration and
    -- returns the float version of it.

    -- Examples:
    --
    -- >>> parseTest parseDuration "4"
    -- 0.25
    --
    -- >>> parseTest parseDuration "4."
    -- 0.375

    parseDuration :: Parser Double
    parseDuration = CM.liftM2
                    (\nums extensions ->
                     let num = (read :: String -> Double) nums
                     in
                       foldr
                       (*)
                       (1 / num)
                       [1.5 | _ <- [1..(length extensions)]])
                    (option "4" (some digitChar))
                    (many (string "."))

    -- 'parseChordOrNote' takes a string representation of either a note or
    -- chord and returns a tuple of the base note and the steps of the chord.

    -- Examples:
    --
    -- >>> parseTest parseChordOrNote "C"
    -- (NoteHelp "C" "" 0 "", [0])
    --
    -- >>> parseTest parseChordOrNote "Cm"
    -- (NoteHelp "C" "" 0 "", [0, 3, 7])

    parseChordOrNote :: Parser (NoteHelp, [Int])
    parseChordOrNote = CM.liftM2
                       (\p chord ->
                        let stuff =
                                case chord of
                                  "" -> [0]
                                  "M" -> [0, 4, 7]
                                  "m" -> [0, 3, 7]
                                  "M7" -> [0, 4, 7, 11]
                                  "m7" -> [0, 3, 7, 11]
                                  "dim" -> [0, 3, 6]
                                  "aug" -> [0, 4, 8]
                                  "aug7" -> [0, 4, 8, 11]
                                  "sus" -> [0, 5, 7]
                                  "add9" -> [0, 4, 7, 10, 13]
                                  _ -> errorWithoutStackTrace
                                       ("Unrecognized chord type "
                                        ++ chord)
                        in
                          (p, stuff))
                       parseNote
                       (many alphaNumChar)

    parseMode :: Parser (NoteHelp, ScaleType)
    parseMode = CM.liftM2
                (\p mode ->
                 let stuff =
                         case mode of
                           "M" -> Major
                           "m" -> Minor
                           "Ae" -> Æolian
                           "Lo" -> Locrian
                           "Io" -> Major
                           "Do" -> Dorian
                           "Ph" -> Phrygian
                           "Ly" -> Lydian
                           "Mi" -> Mixolydian
                           _    -> errorWithoutStackTrace ("Unrecognized mode " ++ mode)
                 in
                   (p,stuff))
                parseNote
                (many letterChar)

    -- |'parseBeat' parses a beat sequence.

    -- |Examples:
    --
    -- >>> parseTest parseBeat "cm8"
    -- BeatHelp "cm" 0.125

    parseBeat :: Parser NoteHelp
    parseBeat = CM.liftM2
                BeatHelp
                (many letterChar)
                parseDuration

    -- 'parseCommandsFromFile' parses a file of Musik commands.

    -- See 'parseMusikFile'

    parseCommandsFromFile :: Parser [Command]
    parseCommandsFromFile = CM.liftM3
                            (const const)
                            (optional parseSep)
                            (parseCommandOrAtom `sepEndBy` parseSep)
                            eof

    -- 'getParsed' decides whether to get standard input
    -- or to open a file, parses whatever it is, and returns
    -- it.

    getParsed :: FilePath
              -> IO (Either (ParseError Char Void) [Command])
    getParsed f = fmap
                  (parse parseCommandsFromFile "")
                  (case f of
                     "-" -> putStrLn "Reading from standard input"
                            >> getContents
                     _ -> readFile f)

    -- |'parseMusikFile' takes a path to a Musik file and parses the file at
    -- that location, returning a list of 'Command'. This function is the
    -- most useful out of all of the ones exported here.

    parseMusikFile :: FilePath -> IO [Command]
    parseMusikFile f =
        getParsed f >>=
        either
        ((>> return []) . errorWithoutStackTrace . show)
        return


    -- |'parseChord' takes a scale and a string and returns a list of
    -- integers representing note frequencies.

    -- |Examples:
    --
    -- >>> let (ChordHelp b) = parseChord notes "Cm" in putStrLn (show b)
    -- [60,63,67]
    --
    -- >>> let (ChordHelp b) = parseChord notes "C+E+G+B" in putStrLn (show b)
    -- [60,64,67,71]

    parseChord :: Scale -> String -> ChordHelp
    parseChord scale s =
        let a = splitOn "+" s
            chordStuff = map
                         (either
                          (errorWithoutStackTrace . (++) "ERROR: " . show)
                          id . parse parseChordOrNote "")
                         a
            convertedNotes = map
                             (convertNoteHelpToDat scale . fst)
                             chordStuff
            mappings = map snd chordStuff
            notesToPlay = map (\(Note note _) -> note) convertedNotes
        in
          ChordHelp
          (concat
           (zipWith
            (map . (+))
            notesToPlay mappings))

    -- 'convertNoteHelpToDat' takes a scale and a 'NoteHelp' and
    -- returns a better representation of a note ('Note').

    -- Examples:
    --
    -- >>> do { a <- notes; convertNoteHelpToDat a (NoteHelp "C" "" 0 "2.-4") }
    -- Note 60 1.0
    --
    -- >>> do { a <- notes; convertNoteHelpToDat a (NoteHelp "G" "" (-1) "4.") }
    -- Note 55 0.375

    convertNoteHelpToDat :: Scale -> NoteHelp -> Dat
    convertNoteHelpToDat s (NoteHelp note accidental octave duration) =
          let i = s `S.index` (HM.!) stringToNote note
              j = i
                  + foldr ((+) . ifthenelse 1 (-1) . (== '#')) 0 accidental
                  + (12 * octave)
              k = splitOn "-" duration
              dur = foldr
                    ((+) .
                     either
                     (errorWithoutStackTrace . (++) "Unexpected: " . show)
                     id . parse parseDuration "") 0 k
          in
            Note j dur
    convertNoteHelpToDat _ _ = Emp

    -- 'convertBeatHelpToDat' converts a 'BeatHelp' to 'Dat' by retrieving
    -- the appropriate mapping from beat sequence specification to note.

    convertBeatHelpToDat :: Beats -> NoteHelp -> Dat
    convertBeatHelpToDat beats (BeatHelp s d)
        | HM.member (map toLower s) beats = Note ((HM.!) beats (map toLower s)) d
        | otherwise = errorWithoutStackTrace ("Unrecognized beat specification: " ++ s)
    convertBeatHelpToDat _ _ = Emp

    -- |'convertNoteHelpToNoteOrBeat' takes a 'NoteHelp' and converts it
    -- to a 'Dat' by calling the appropriate helper function.

    convertNoteHelpToNoteOrBeat :: Scale -> Beats ->
                                   NoteHelp -> Dat
    convertNoteHelpToNoteOrBeat s _ n@NoteHelp{} =
        convertNoteHelpToDat s n
    convertNoteHelpToNoteOrBeat _ beats b@BeatHelp{} =
        convertBeatHelpToDat beats b
