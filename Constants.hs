{-
  Copyright 2010–2015 Chiraag M. Nataraj
  This file is part of Musik.
  
  Musik is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

-- |Constants contains a bunch of constants and variables used throughout Musik

module Constants where

    import Datatypes
    import qualified Data.Foldable as F
    import qualified Data.HashMap.Lazy as HM
    import qualified Data.Sequence as S
    
    -- *Version-related constants

    -- |'copyright' stores the string printed at the beginning of the program.
    copyright :: String
    copyright = "Copyright (C) 2010-2016 \
                 \Chiraag M. Nataraj\nMusik version " ++
                show version

    -- |'version' stores the current version of Musik.
              
    version :: Float
    version = 3.1

    -- |'beginParse' is the string printed at the beginning of parsing.
              
    beginParse :: String
    beginParse = "Beginning parsing..."

    -- |'endParse' is the string printed at the end of parsing.
                 
    endParse :: String
    endParse = "Successfully parsed file."

    -- |'beginInterpret' is the string printed at the beginning of interpreting.
               
    beginInterpret :: String
    beginInterpret = "Interpreting file..."

    -- |'endInterpret' is the string printed at the end of interpreting.
                     
    endInterpret :: String
    endInterpret = "Successfully interpreted file."

    -- |'beginExport' is the string printed at the beginning of exporting.
                   
    beginExport :: String
    beginExport = "Exporting file..."

    -- |'endExport' is the string printed at the end of exporting.
                  
    endExport :: String
    endExport = "Successfully exported file."

    -- *Constants

    -- |'notes' stores the current mapping from \"C\", \"D\", etc. to actual
    -- MIDI pitches. This value will change when using \\transpose and
    -- \\scale.

    notes :: Scale
    -- notes = listArray (0, 7) [0,60,62,64,65,67,69,71]
    notes = S.fromList [0,60,62,64,65,67,69,71]

    -- |'order' stores the order of elements in 'notes'.
            
    order :: S.Seq String
    order = S.fromList ["R","C","D","E","F","G","A","B"]

    -- |'stringToX' takes a function that transforms a 'Scale' into a '[Int]'
    -- and a 'Scale' and returns a HashMap. Convenience function used to
    -- define 'stringToMidi' and 'stringToNote'.
            
    stringToX :: (Scale -> S.Seq Int) -> Scale -> HM.HashMap String Int
    stringToX = (.) (HM.fromList . F.toList . S.zip order)
    
    -- |'stringToMidi' contains a static mapping of notes to MIDI notes.
    -- This is used to determine the base note for deriving scales and so
    -- should not change when 'notes' does.
                   
    stringToMidi :: HM.HashMap String Int
    stringToMidi = stringToX id notes

    -- |'stringToNote' maps note names such as \"C\" to an index in the
    -- 'notes' array.
            
    stringToNote :: HM.HashMap String Int
    stringToNote = stringToX (\x -> S.fromList [0..length x]) notes

    -- |'tpb' stores the number of ticks per beat

    tpb :: Int
    tpb = 3780

    -- *Variables

    -- |'drums' contains the default mapping used for the beat
    -- generation command.
                  
    drums :: Beats
    drums = HM.fromList
            [("abd", 35)
            ,("bd", 36)
            ,("ss", 37)
            ,("as", 38)
            ,("hc", 39)
            ,("es", 40)
            ,("lft", 41)
            ,("chh", 42)
            ,("hft", 43)
            ,("phh", 44)
            ,("lt", 45)
            ,("ohh", 46)
            ,("lmt", 47)
            ,("hmt", 48)
            ,("cco", 49)
            ,("ht", 50)
            ,("rco", 51)
            ,("chc", 52)
            ,("rb", 53)
            ,("t", 54)
            ,("sc", 55)
            ,("c", 56)
            ,("cct", 57)
            ,("v", 58)
            ,("rct", 59)
            ,("hb", 60)
            ,("lb", 61)
            ,("mhc", 62)
            ,("ohc", 63)
            ,("lc", 64)
            ,("hti", 65)
            ,("lti", 66)
            ,("ha", 67)
            ,("la", 68)
            ,("ca", 69)
            ,("m", 70)
            ,("sw", 71)
            ,("lw", 72)
            ,("sg", 73)
            ,("lg", 74)
            ,("cl", 75)
            ,("hwb", 76)
            ,("lwb", 77)
            ,("mc", 78)
            ,("oc", 79)
            ,("mt", 80)
            ,("ot", 81)]

    -- |'instruments' contains the default mapping of instrument names to
    -- MIDI patch numbers.
                   
    instruments :: Insts
    instruments = HM.fromList
                  [("PIANO", 0),
                   ("ACOUSTIC_GRAND", 0),
                   ("BRIGHT_ACOUSTIC", 1),
                   ("ELECTRIC_GRAND", 2),
                   ("HONKEY_TONK", 3),
                   ("ELECTRIC_PIANO", 4),
                   ("ELECTRIC_PIANO_1", 4),
                   ("ELECTRIC_PIANO_2", 5),
                   ("HARPISCHORD", 6),
                   ("CLAVINET", 7),
                   ("CELESTA", 8),
                   ("GLOCKENSPIEL", 9),
                   ("MUSIC_BOX", 10),
                   ("VIBRAPHONE", 11),
                   ("MARIMBA", 12),
                   ("XYLOPHONE", 13),
                   ("TUBULAR_BELLS", 14),
                   ("DULCIMER", 15),
                   ("DRAWBAR_ORGAN", 16),
                   ("PERCUSSIVE_ORGAN", 17),
                   ("ROCK_ORGAN", 18),
                   ("CHURCH_ORGAN", 19),
                   ("REED_ORGAN", 20),
                   ("ACCORDIAN", 21),
                   ("HARMONICA", 22),
                   ("TANGO_ACCORDIAN", 23),
                   ("GUITAR", 24),
                   ("NYLON_STRING_GUITAR", 24),
                   ("STEEL_STRING_GUITAR", 25),
                   ("ELECTRIC_JAZZ_GUITAR", 26),
                   ("ELECTRIC_CLEAN_GUITAR", 27),
                   ("ELECTRIC_MUTED_GUITAR", 28),
                   ("OVERDRIVEN_GUITAR", 29),
                   ("DISTORTION_GUITAR", 30),
                   ("GUITAR_HARMONICS", 31),
                   ("ACOUSTIC_BASS", 32),
                   ("ELECTRIC_BASS_FINGER", 33),
                   ("ELECTRIC_BASS_PICK", 34),
                   ("FRETLESS_BASS", 35),
                   ("SLAP_BASS_1", 36),
                   ("SLAP_BASS_2", 37),
                   ("SYNTH_BASS_1", 38),
                   ("SYNTH_BASS_2", 39),
                   ("VIOLIN", 40),
                   ("VIOLA", 41),
                   ("CELLO", 42),
                   ("CONTRABASS", 43),
                   ("TREMOLO_STRINGS", 44),
                   ("PIZZICATO_STRINGS", 45),
                   ("ORCHESTRAL_STRINGS", 46),
                   ("TIMPANI", 47),
                   ("STRING_ENSEMBLE_1", 48),
                   ("STRING_ENSEMBLE_2", 49),
                   ("SYNTH_STRINGS_1", 50),
                   ("SYNTH_STRINGS_2", 51),
                   ("CHOIR_AAHS", 52),
                   ("VOICE_OOHS", 53),
                   ("SYNTH_VOICE", 54),
                   ("ORCHESTRA_HIT", 55),
                   ("TRUMPET", 56),
                   ("TROMBONE", 57),
                   ("TUBA", 58),
                   ("MUTED_TRUMPET", 59),
                   ("FRENCH_HORN", 60),
                   ("BRASS_SECTION", 61),
                   ("SYNTHBRASS_1", 62),
                   ("SYNTH_BRASS_1", 62),
                   ("SYNTHBRASS_2", 63),
                   ("SYNTH_BRASS_2", 63),
                   ("SOPRANO_SAX", 64),
                   ("ALTO_SAX", 65),
                   ("TENOR_SAX", 66),
                   ("BARITONE_SAX", 67),
                   ("OBOE", 68),
                   ("ENGLISH_HORN", 69),
                   ("BASSOON", 70),
                   ("CLARINET", 71),
                   ("PICCOLO", 72),
                   ("FLUTE", 73),
                   ("RECORDER", 74),
                   ("PAN_FLUTE", 75),
                   ("BLOWN_BOTTLE", 76),
                   ("SKAKUHACHI", 77),
                   ("WHISTLE", 78),
                   ("OCARINA", 79),
                   ("LEAD_SQUARE", 80),
                   ("SQUARE", 80),
                   ("LEAD_SAWTOOTH", 81),
                   ("SAWTOOTH", 81),
                   ("LEAD_CALLIOPE", 82),
                   ("CALLIOPE", 82),
                   ("LEAD_CHIFF", 83),
                   ("CHIFF", 83),
                   ("LEAD_CHARANG", 84),
                   ("CHARANG", 84),
                   ("LEAD_VOICE", 85),
                   ("VOICE", 85),
                   ("LEAD_FIFTHS", 86),
                   ("FIFTHS", 86),
                   ("LEAD_BASSLEAD", 87),
                   ("BASSLEAD", 87),
                   ("PAD_NEW_AGE", 88),
                   ("NEW_AGE", 88),
                   ("PAD_WARM", 89),
                   ("WARM", 89),
                   ("PAD_POLYSYNTH", 90),
                   ("POLYSYNTH", 90),
                   ("PAD_CHOIR", 91),
                   ("CHOIR", 91),
                   ("PAD_BOWED", 92),
                   ("BOWED", 92),
                   ("PAD_METALLIC", 93),
                   ("METALLIC", 93),
                   ("PAD_HALO", 94),
                   ("HALO", 94),
                   ("PAD_SWEEP", 95),
                   ("SWEEP", 95),
                   ("FX_RAIN", 96),
                   ("RAIN", 96),
                   ("FX_SOUNDTRACK", 97),
                   ("SOUNDTRACK", 97),
                   ("FX_CRYSTAL", 98),
                   ("CRYSTAL", 98),
                   ("FX_ATMOSPHERE", 99),
                   ("ATMOSPHERE", 99),
                   ("FX_BRIGHTNESS", 100),
                   ("BRIGHTNESS", 100),
                   ("FX_GOBLINS", 101),
                   ("GOBLINS", 101),
                   ("FX_ECHOES", 102),
                   ("ECHOES", 102),
                   ("FX_SCI-FI", 103),
                   ("SCI-FI", 103),
                   ("SITAR", 104),
                   ("BANJO", 105),
                   ("SHAMISEN", 106),
                   ("KOTO", 107),
                   ("KALIMBA", 108),
                   ("BAGPIPE", 109),
                   ("FIDDLE", 110),
                   ("SHANAI", 111),
                   ("TINKLE_BELL", 112),
                   ("AGOGO", 113),
                   ("STEEL_DRUMS", 114),
                   ("WOODBLOCK", 115),
                   ("TAIKO_DRUM", 116),
                   ("MELODIC_TOM", 117),
                   ("SYNTH_DRUM", 118),
                   ("REVERSE_CYMBAL", 119),
                   ("GUITAR_FRET_NOISE", 120),
                   ("BREATH_NOISE", 121),
                   ("SEASHORE", 122),
                   ("BIRD_TWEET", 123),
                   ("TELEPHONE_RING", 124),
                   ("HELICOPTER", 125),
                   ("APPLAUSE", 126),
                   ("GUNSHOT", 127)]
