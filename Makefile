CC="ghc"
CCGENARGS=-Wall -funbox-strict-fields -XUnboxedTuples -funfolding-use-threshold=100 -O3 -optc-O3 -j8
CCPROFARGS=-rtsopts -prof -auto-all -caf-all -ddump-simpl -ddump-stranal -ddump-to-file -dppr-case-as-let

all: bin doc

bin: *.hs
	ghc $(CCGENARGS) -o musik Musik.hs

doc: *.hs
	./docgen.sh

clean:
	rm -f *.hi *.o *.dump-* musik
	rm -rf doc/

test: bin
	./musik -v tests/test.musik

android: *.hs
	ghc -Wall -funbox-strict-fields -O2 -static -optl-static -optl-pthread -o musik Musik.hs

profiling: *.hs
	ghc $(CCGENARGS) $(CCPROFARGS) -o musik Musik.hs

efficiency: bin
	./efficiency.sh

heap-profiling: profiling
	./musik +RTS -hb -RTS -v efficiency.musik && hp2ps -c musik.hp && ps2pdf musik.ps

time-profiling: profiling
	./musik +RTS -p -RTS efficiency.musik
