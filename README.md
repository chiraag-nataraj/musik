# Musik
Musik is a program which takes text such as `C4 D4 E4 F4 G4 A4 B4 C^4` and translates it into audible music.

# Features

* Basic Notes (C, D, E, etc.)
* Octaves (^ and _)
* Sharps and Flats (# and b)
* Chords - Predefined:
  * Major
  * Minor
  * Major 7th
  * Minor 7th
  * Diminished
  * Sustained
  * Augmented
  * Augmented 7th
  * Add9
* Chords - user-made
* Instrument changes - both in melody and in chords
* Multiple melody notes at the same time
* Scale changes
* Tempo changes
* Sections - to avoid retyping the same thing over and over again
* Emulate holding down the sustain pedal
* Tie notes together
* Simple beat specification using MIDI channel 10
* Custom instrument mappings (to access sounds in expanded sound banks)
* Custom beat symbol mappings (to access sounds other than the ones mapped by default)
* Per-file option to specify the file to save the music to
* Allows you to split your music into multiple musik files and import them

# Building

To build Musik from source, you will need the following programs:

* ghc
* GNU Make

You will also need the following Haskell packages:

* HCodecs (provides `Codec.Midi`)
* megaparsec (provides `Text.Megaparsec`)
* text (provides `Data.Text`)
* unordered-containers (provides `Data.HashMap`)
* containers (provides `Data.Sequence`)
* mtl (provides `Control.Monad.State`)
* hashable (provides `Data.Hashable`)
* dlist (provides `Data.DList`)
* split (provides `Data.List.Split`)

You can install all of these dependencies in Debian sid by using the following set of commands:
```
sudo aptitude install haskell-platform libghc-megaparsec-dev libghc-unordered-containers-dev libghc-hashable-dev libghc-dlist-dev libghc-split-dev
cabal install HCodecs
```

After installing the dependencies, a simple `make` should generate the `musik` binary.
